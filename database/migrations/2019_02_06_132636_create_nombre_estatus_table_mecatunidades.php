<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNombreEstatusTableMecatunidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mecatunidades', function (Blueprint $table) {
            $table->string('udnombre')->unique()->after('id');
            $table->enum('usestatus',['Activo','Inactivo'])->after('udnombre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mecatunidades', function (Blueprint $table) {
            //
        });
    }
}
