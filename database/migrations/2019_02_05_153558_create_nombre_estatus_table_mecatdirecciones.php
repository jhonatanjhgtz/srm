<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNombreEstatusTableMecatdirecciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mecatdirecciones', function (Blueprint $table) {
            $table->string('nombre')->unique()->after('id');
            $table->enum('estatus',['Activo','Inactivo'])->after('nombre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mecatdirecciones', function (Blueprint $table) {
            //
        });
    }
}
