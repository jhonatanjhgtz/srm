<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetaNombreDireccionProgramaProyectoUnidadmedida extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('memetas', function (Blueprint $table) {
            $table->string('memeta')->unique()->after('id');
            $table->string('menombre')->after('memeta');
            $table->unsignedInteger('id_unidad_medida');
            $table->unsignedInteger('id_direccion');
            $table->unsignedInteger('id_programa');
            $table->unsignedInteger('id_proyecto');
            $table->foreign('id_unidad_medida')->references('id')->on('mecatunidades');
            $table->foreign('id_direccion')->references('id')->on('mecatdirecciones');
            $table->foreign('id_programa')->references('id')->on('meprogramas');
            $table->foreign('id_proyecto')->references('id')->on('meproyectos');
            $table->enum('meestatus',['Activo','Inactivo']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('memetas', function (Blueprint $table) {
            //
        });
    }
}
