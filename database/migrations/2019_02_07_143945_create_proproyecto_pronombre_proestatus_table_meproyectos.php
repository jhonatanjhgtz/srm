<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProproyectoPronombreProestatusTableMeproyectos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meproyectos', function (Blueprint $table) {
            $table->string('proproyecto')->unique()->after('id');
            $table->string('pronombre')->after('proprograma');
            $table->enum('proestatus',['Activo','Inactivo'])->after('pronombre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meproyectos', function (Blueprint $table) {
            //
        });
    }
}
