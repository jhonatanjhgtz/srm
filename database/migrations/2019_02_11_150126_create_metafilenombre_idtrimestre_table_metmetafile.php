<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetafilenombreIdtrimestreTableMetmetafile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metmetafile', function (Blueprint $table) {
            $table->mediumText('archnombre')->after('id');
            $table->integer('id_trimestre')->after('archnombre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metmetafile', function (Blueprint $table) {
            //
        });
    }
}
