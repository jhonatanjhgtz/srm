<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramaNombreEstatusTableMeprogramas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meprogramas', function (Blueprint $table) {
            $table->string('progprograma')->unique()->after('id');
            $table->string('prognombre')->after('progprograma');
            $table->enum('progestatus',['Activo','Inactivo'])->after('prognombre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meprogramas', function (Blueprint $table) {
            //
        });
    }
}
