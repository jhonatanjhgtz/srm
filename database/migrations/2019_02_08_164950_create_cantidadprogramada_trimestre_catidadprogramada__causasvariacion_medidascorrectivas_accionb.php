<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCantidadprogramadaTrimestreCatidadprogramadaCausasvariacionMedidascorrectivasAccionb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metrimestes_meta', function (Blueprint $table) {
            $table->integer('tricantidad_rpogramada')->after('id');
            $table->string('tritrimestre')->after('tricantidad_rpogramada');
            $table->mediumText('tricausas_variacion')->after('tritrimestre');
            $table->mediumText('trimedidas_correctivas')->after('tricausas_variacion');
            $table->mediumText('triaccionb')->after('trimedidas_correctivas');
            $table->string('trievidencia')->after('triaccionb');
            $table->unsignedInteger('id_meta');
            $table->foreign('id_meta')->references('id')->on('memetas');
            $table->enum('triestatus',['Activo','Inactivo']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metrimestes_meta', function (Blueprint $table) {
            //
        });
    }
}
