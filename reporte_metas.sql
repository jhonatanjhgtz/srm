-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-06-2019 a las 18:16:41
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `reporte_metas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mecatdirecciones`
--
create database reporte_metas;
use reporte_metas;

CREATE TABLE `mecatdirecciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estatus` enum('Activo','Inactivo') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `mecatdirecciones`
--

INSERT INTO `mecatdirecciones` (`id`, `nombre`, `estatus`, `created_at`, `updated_at`) VALUES
(1, 'Asistencia Técnica', 'Activo', '2019-02-05 21:39:35', '2019-02-07 19:25:11'),
(2, 'Desarrollo Tecnológico', 'Activo', '2019-02-05 23:07:18', '2019-02-05 23:07:18'),
(3, 'Financiamiento', 'Activo', '2019-02-05 23:07:45', '2019-02-05 23:07:45'),
(4, 'Fiscalización', 'Activo', '2019-02-05 23:08:26', '2019-02-05 23:08:26'),
(5, 'Investigación Cientifica', 'Activo', '2019-02-05 23:08:55', '2019-02-05 23:08:55'),
(6, 'Normatividad', 'Activo', '2019-02-05 23:09:16', '2019-02-05 23:09:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mecatunidades`
--

CREATE TABLE `mecatunidades` (
  `id` int(10) UNSIGNED NOT NULL,
  `udnombre` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `udestatus` enum('Activo','Inactivo') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `mecatunidades`
--

INSERT INTO `mecatunidades` (`id`, `udnombre`, `udestatus`, `created_at`, `updated_at`) VALUES
(1, 'Documentos', 'Activo', '2019-02-06 20:40:04', '2019-02-06 20:40:04'),
(7, 'Reporte', 'Activo', '2019-02-06 22:58:32', '2019-02-06 22:58:32'),
(8, 'Sistema', 'Activo', '2019-02-06 22:58:50', '2019-02-06 22:58:50'),
(9, 'Beca', 'Activo', '2019-02-06 22:59:09', '2019-02-06 22:59:09'),
(10, 'Persona', 'Activo', '2019-02-06 23:03:19', '2019-02-06 23:03:19'),
(11, 'Evento', 'Activo', '2019-02-06 23:04:11', '2019-02-06 23:04:11'),
(12, 'Proyecto', 'Activo', '2019-02-06 23:04:17', '2019-02-06 23:04:17'),
(13, 'Asesoría', 'Activo', '2019-02-06 23:04:22', '2019-02-06 23:04:22'),
(14, 'Curso', 'Activo', '2019-02-06 23:04:28', '2019-02-06 23:04:28'),
(15, 'Beneficiario', 'Activo', '2019-02-06 23:04:38', '2019-02-06 23:04:38'),
(16, 'Base de Datos', 'Activo', '2019-02-06 23:04:47', '2019-02-06 23:04:47'),
(17, 'Acuerdo', 'Activo', '2019-02-06 23:04:54', '2019-02-06 23:16:51'),
(18, 'Transferencia', 'Activo', '2019-02-06 23:04:59', '2019-02-06 23:04:59'),
(19, 'Revista', 'Activo', '2019-02-06 23:05:06', '2019-02-06 23:05:06'),
(20, 'Boletín', 'Activo', '2019-02-06 23:05:11', '2019-02-06 23:05:11'),
(21, 'Publicación', 'Activo', '2019-02-06 23:05:17', '2019-02-06 23:05:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `memetas`
--

CREATE TABLE `memetas` (
  `id` int(10) UNSIGNED NOT NULL,
  `memeta` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menombre` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_unidad_medida` int(10) UNSIGNED NOT NULL,
  `id_direccion` int(10) UNSIGNED NOT NULL,
  `id_programa` int(10) UNSIGNED NOT NULL,
  `id_proyecto` int(10) UNSIGNED NOT NULL,
  `meestatus` enum('Activo','Inactivo') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `memetas`
--

INSERT INTO `memetas` (`id`, `memeta`, `menombre`, `created_at`, `updated_at`, `id_unidad_medida`, `id_direccion`, `id_programa`, `id_proyecto`, `meestatus`) VALUES
(43, '001', 'TEST', '2019-05-09 17:19:45', '2019-05-09 17:19:45', 1, 2, 2, 1, 'Activo'),
(44, '11', 'Realizar una prueba', '2019-05-09 20:03:33', '2019-05-09 20:03:33', 8, 4, 1, 1, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `meprogramas`
--

CREATE TABLE `meprogramas` (
  `id` int(10) UNSIGNED NOT NULL,
  `progprograma` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prognombre` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `progestatus` enum('Activo','Inactivo') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `meprogramas`
--

INSERT INTO `meprogramas` (`id`, `progprograma`, `prognombre`, `progestatus`, `created_at`, `updated_at`) VALUES
(1, '00000001', 'Innovación Científica y Tecnológica', 'Activo', '2019-02-07 18:49:28', '2019-02-07 20:06:26'),
(2, '00000002', 'Investigación Cientifica', 'Activo', '2019-02-07 20:07:07', '2019-02-07 20:09:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `meproyectos`
--

CREATE TABLE `meproyectos` (
  `id` int(10) UNSIGNED NOT NULL,
  `proproyecto` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pronombre` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `proestatus` enum('Activo','Inactivo') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `meproyectos`
--

INSERT INTO `meproyectos` (`id`, `proproyecto`, `pronombre`, `proestatus`, `created_at`, `updated_at`) VALUES
(1, '030801010102', 'Normatividad para el desarrollo de la Ciencia y Tecnología', 'Activo', '2019-02-07 22:43:37', '2019-02-07 22:52:06'),
(2, '030804010101', 'Asistencia Técnica a la Innovación', 'Activo', '2019-02-07 22:53:03', '2019-02-07 22:53:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metmetafiles`
--

CREATE TABLE `metmetafiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `archnombre` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_trimestre` int(11) NOT NULL,
  `cantidad_programada_mes` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `metmetafiles`
--

INSERT INTO `metmetafiles` (`id`, `archnombre`, `id_trimestre`, `cantidad_programada_mes`, `created_at`, `updated_at`) VALUES
(2, '1558629918spiderman.png', 6, 2, '2019-05-23 16:44:51', '2019-05-23 16:45:18'),
(3, '1558630759deadpool.png', 2, 2, '2019-05-23 16:59:19', '2019-05-23 16:59:19'),
(4, '1558630771spiderman.png', 2, 3, '2019-05-23 16:59:31', '2019-05-23 16:59:39'),
(5, '1558630878spiderman.png', 3, 4, '2019-05-23 17:01:18', '2019-05-23 17:01:18'),
(6, '1558630891ironman.png', 3, 5, '2019-05-23 17:01:31', '2019-05-23 17:03:31'),
(7, '1546370047pdf-test.pdf', 5, 8, '2019-01-01 19:14:07', '2019-01-01 19:14:07'),
(8, '1549064883deadpool.png', 5, 10, '2019-02-01 23:48:03', '2019-02-01 23:48:03'),
(10, '1551484208ironman.png', 5, 2, '2019-03-01 23:50:08', '2019-03-01 23:50:08'),
(11, '1554162651pdf-test.pdf', 6, 2, '2019-04-01 23:50:51', '2019-04-01 23:50:51'),
(12, '1559429498deadpool.png', 6, 2, '2019-06-01 22:51:38', '2019-06-01 22:51:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metrimestres_metas`
--

CREATE TABLE `metrimestres_metas` (
  `id` int(10) UNSIGNED NOT NULL,
  `tricantidad_rpogramada` int(11) DEFAULT NULL,
  `tritrimestre` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tricausas_variacion` mediumtext COLLATE utf8mb4_unicode_ci,
  `trimedidas_correctivas` mediumtext COLLATE utf8mb4_unicode_ci,
  `triaccionb` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_meta` int(10) UNSIGNED NOT NULL,
  `triestatus` enum('Activo','Inactivo') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `metrimestres_metas`
--

INSERT INTO `metrimestres_metas` (`id`, `tricantidad_rpogramada`, `tritrimestre`, `tricausas_variacion`, `trimedidas_correctivas`, `triaccionb`, `created_at`, `updated_at`, `id_meta`, `triestatus`) VALUES
(1, 10, 'Primer Trimestre', NULL, NULL, NULL, '2019-05-09 17:19:45', '2019-05-15 18:06:45', 43, 'Activo'),
(2, 8, 'Segundo Trimestre', NULL, NULL, NULL, '2019-05-09 17:19:45', '2019-05-15 18:11:50', 43, 'Activo'),
(3, 10, 'Tercer Trimestre', NULL, NULL, NULL, '2019-05-09 17:19:45', '2019-05-23 17:00:47', 43, 'Activo'),
(4, NULL, 'Cuarto Trimestre', NULL, NULL, NULL, '2019-05-09 17:19:45', '2019-05-09 17:19:45', 43, 'Activo'),
(5, 20, 'Primer Trimestre', NULL, NULL, NULL, '2019-05-09 20:03:33', '2019-01-01 19:11:55', 44, 'Activo'),
(6, 6, 'Segundo Trimestre', NULL, NULL, NULL, '2019-05-09 20:03:33', '2019-05-13 14:27:48', 44, 'Activo'),
(7, 1, 'Tercer Trimestre', NULL, NULL, NULL, '2019-05-09 20:03:33', '2019-05-13 14:07:47', 44, 'Activo'),
(8, 6, 'Cuarto Trimestre', NULL, NULL, NULL, '2019-05-09 20:03:33', '2019-05-13 18:43:43', 44, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_02_05_123922_create_table_direcciones', 2),
(4, '2019_02_05_124532_agregar_campos_table_direcciones', 3),
(5, '2019_02_05_130313_cambiar_campos_table_direcciones', 4),
(6, '2019_02_05_152745_create_table_mecatdirecciones', 5),
(7, '2019_02_05_152838_create_nombre_estatus_table_mecatdirecciones', 5),
(8, '2019_02_05_153558_create_nombre_estatus_table_mecatdirecciones', 6),
(9, '2019_02_06_132442_create_table_mecatunidades', 7),
(10, '2019_02_06_132636_create_nombre_estatus_table_mecatunidades', 7),
(11, '2019_02_06_174242_create_table_meprogramas', 8),
(12, '2019_02_06_175752_create_table_meprogramas', 9),
(13, '2019_02_06_175836_create_programa_nombre_estatus_table_meprogramas', 9),
(14, '2019_02_07_143354_create_table_meproyectos', 10),
(15, '2019_02_07_143945_create_proproyecto_pronombre_proestatus_table_meproyectos', 10),
(16, '2019_02_08_110210_create_table_memetas', 11),
(17, '2019_02_08_114333_create_table_memetas', 12),
(18, '2019_02_08_114347_create_meta_nombre_programadoxtrimestre_causasvariacion_medidadcorrectivas_accionb', 12),
(19, '2019_02_08_121246_create_table_memetas', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('gtzjhonatan@gmail.com', '$2y$10$imQ23yTWVodGO4sF4bw90.D4F6nOH40/nymjgPmWHjaJCDqErU8Se', '2019-05-09 03:01:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 'gtzjhonatan@gmail.com', '$2y$10$0X7TsvdPtFqQVMN.oFeCKeD1AcCwVBZTkbIfxuQy/kQtbN0I2j6J2', 'AEJZNisbBqc0loO3kYoVGMFtGiJwFbuFJiioqpAYF9U7JoFoJIZ4z9uhDnn9', '2019-01-30 20:48:45', '2019-05-08 15:32:41');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `mecatdirecciones`
--
ALTER TABLE `mecatdirecciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mecatdirecciones_nombre_unique` (`nombre`);

--
-- Indices de la tabla `mecatunidades`
--
ALTER TABLE `mecatunidades`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mecatunidades_udnombre_unique` (`udnombre`);

--
-- Indices de la tabla `memetas`
--
ALTER TABLE `memetas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `memetas_memeta_unique` (`memeta`),
  ADD KEY `memetas_id_unidad_medida_foreign` (`id_unidad_medida`),
  ADD KEY `memetas_id_direccion_foreign` (`id_direccion`),
  ADD KEY `memetas_id_programa_foreign` (`id_programa`),
  ADD KEY `memetas_id_proyecto_foreign` (`id_proyecto`);

--
-- Indices de la tabla `meprogramas`
--
ALTER TABLE `meprogramas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `meprogramas_progprograma_unique` (`progprograma`);

--
-- Indices de la tabla `meproyectos`
--
ALTER TABLE `meproyectos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `meproyectos_proprograma_unique` (`proproyecto`);

--
-- Indices de la tabla `metmetafiles`
--
ALTER TABLE `metmetafiles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `metrimestres_metas`
--
ALTER TABLE `metrimestres_metas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `metrimestes_meta_id_meta_foreign` (`id_meta`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `mecatdirecciones`
--
ALTER TABLE `mecatdirecciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `mecatunidades`
--
ALTER TABLE `mecatunidades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `memetas`
--
ALTER TABLE `memetas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `meprogramas`
--
ALTER TABLE `meprogramas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `meproyectos`
--
ALTER TABLE `meproyectos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `metmetafiles`
--
ALTER TABLE `metmetafiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `metrimestres_metas`
--
ALTER TABLE `metrimestres_metas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `memetas`
--
ALTER TABLE `memetas`
  ADD CONSTRAINT `memetas_id_direccion_foreign` FOREIGN KEY (`id_direccion`) REFERENCES `mecatdirecciones` (`id`),
  ADD CONSTRAINT `memetas_id_programa_foreign` FOREIGN KEY (`id_programa`) REFERENCES `meprogramas` (`id`),
  ADD CONSTRAINT `memetas_id_proyecto_foreign` FOREIGN KEY (`id_proyecto`) REFERENCES `meproyectos` (`id`),
  ADD CONSTRAINT `memetas_id_unidad_medida_foreign` FOREIGN KEY (`id_unidad_medida`) REFERENCES `mecatunidades` (`id`);

--
-- Filtros para la tabla `metrimestres_metas`
--
ALTER TABLE `metrimestres_metas`
  ADD CONSTRAINT `metrimestes_meta_id_meta_foreign` FOREIGN KEY (`id_meta`) REFERENCES `memetas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
