<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class meprograma extends Model
{
    //campos que queremos modificar
    protected $fillable = ['progprograma','prognombre'];
    public function get_programas(){
        $programas = DB::table('meprogramas')
        ->where('progestatus','=','Activo')
        ->orderby('id','DESC')
        ->get();
        foreach ($programas as $programa) {
            $programa->id =Crypt::encryptString($programa->id);
        }
        return $programas;
    }
}
