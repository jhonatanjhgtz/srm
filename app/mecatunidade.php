<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class mecatunidade extends Model
{
    public function get_unidades(){
        $unidades = DB::table('mecatunidades')
        ->where('udestatus','=','Activo')
        ->orderBy('udnombre','ASC')
        ->get();
        foreach ($unidades as $unidad) {
            $unidad->id = Crypt::encryptString($unidad->id);
            $unidad->udnombre = $unidad->udnombre;
        }
        return $unidades;
    }
}
