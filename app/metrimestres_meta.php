<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class metrimestres_meta extends Model
{
    public function returnDataMetas($procedure,$id_direccion,$rol_user){
        switch ($procedure) {
            case 'ver_metas':
                $validate= 'memetas.id_direccion';
                $id= ($rol_user == 'Administrador') ? '""' : $id_direccion;
                $value = ($rol_user == 'Administrador' ) ? '<>' :'=';
                break;
            case 'update_trimestre':
                $validate= 'memetas.id_direccion';
                $value= '=';
                break;
            default:
                die('--No status--');
                break;
        }
        $meta_allData = DB::table('memetas')
        ->select('mecatdirecciones.nombre as nombre_direccion',
        'memetas.id as id_meta',
        'memetas.memeta as memeta',
        'memetas.menombre as menombre',
        'meprogramas.progprograma as progprograma',
        'meprogramas.prognombre as prognombre',
        'meproyectos.proproyecto as proproyecto',
        'meproyectos.pronombre as pronombre',
        'mecatunidades.udnombre as udnombre')
        ->leftjoin('mecatdirecciones','memetas.id_direccion','=','mecatdirecciones.id')
        ->leftjoin('meprogramas','memetas.id_programa','=','meprogramas.id')
        ->leftjoin('meproyectos','memetas.id_proyecto','=','meproyectos.id')
        ->leftjoin('mecatunidades','memetas.id_unidad_medida','=','mecatunidades.id')
        ->where($validate,$value,$id)
        ->orderBy('memetas.id','DESC')
        //->groupBy('metrimestres_metas.id_meta')
        ->get();
    return $meta_allData;
    }
    public function returnDataMetaTrimestre($id){
        $meta_trimestre = DB::table('metrimestres_metas')
        ->select('mecatdirecciones.nombre as nombre_direccion',
        'metrimestres_metas.trireconduccion',
        'metrimestres_metas.tritrimestre as tritrimestre',
        'metrimestres_metas.tricausas_variacion as tricausas_variacion',
        'metrimestres_metas.trimedidas_correctivas as trimedidas_correctivas',
        'metrimestres_metas.triaccionb as triaccionb',
        'memetas.id as id_meta',
        'memetas.memeta as memeta',
        'memetas.menombre as menombre',
        'meprogramas.progprograma as progprograma',
        'meprogramas.prognombre as prognombre',
        'meproyectos.proproyecto as proproyecto',
        'meproyectos.pronombre as pronombre',
        'mecatunidades.udnombre as udnombre',
        DB::raw('(SELECT trirereconduccion
        FROM metrimestres_metas as mtm
        left outer join metrimestre_reconducciones as mtr ON mtm.id = mtr.id_metrimestres
        WHERE mtr.id_metrimestres = '.$id.' ORDER BY mtr.id DESC LIMIT 1) as reconduccion'))
        ->leftjoin('memetas','memetas.id','=','metrimestres_metas.id_meta')
        ->leftjoin('mecatdirecciones','memetas.id_direccion','=','mecatdirecciones.id')
        ->leftjoin('meprogramas','memetas.id_programa','=','meprogramas.id')
        ->leftjoin('meproyectos','memetas.id_proyecto','=','meproyectos.id')
        ->leftjoin('mecatunidades','memetas.id_unidad_medida','=','mecatunidades.id')
        ->where('metrimestres_metas.id','=',$id)
        ->get();
    return $meta_trimestre;
    }
    public function returnDataMeta($id){
        $meta = DB::table('memetas as met')
        ->leftjoin('mecatdirecciones as dir','met.id_direccion','=','dir.id')
        ->leftjoin('meprogramas as prog','met.id_programa','=','prog.id')
        ->leftjoin('meproyectos as proy','met.id_proyecto','=','proy.id')
        ->leftjoin('mecatunidades as uni','met.id_unidad_medida','=','uni.id')
        ->where('met.id','=',$id)
        ->get();
        return $meta;
    }

    public function returnDataPrimerTrimestre($id){
        $meta_primer_trimestre = DB::table('metrimestres_metas as trim')
        ->select(DB::raw('(select mf.cantidad_programada_mes 
        from metrimestres_metas as tri 
        left join memetas as me on me.id = tri.id_meta 
        left join metmetafiles as mf on tri.id = mf.id_trimestre
        where (mf.created_at BETWEEN "2019-01-01 00:00:00" AND "2019-01-31 23:59:59") AND tri.id_meta="'.$id.'" AND mf.id_trimestre = trim.id)as enero'),
        
        DB::raw('(select mf.cantidad_programada_mes
        from metrimestres_metas as tri 
        left join memetas as me on me.id = tri.id_meta 
        left join metmetafiles as mf on tri.id = mf.id_trimestre
        where (mf.created_at BETWEEN "2019-02-01 00:00:00" AND "2019-02-28 23:59:59") AND tri.id_meta="'.$id.'" AND mf.id_trimestre = trim.id)as febrero'),
        
        DB::raw('(select mf.cantidad_programada_mes
        from metrimestres_metas as tri 
        left join memetas as me on me.id = tri.id_meta 
        left join metmetafiles as mf on tri.id = mf.id_trimestre
        where (mf.created_at BETWEEN "2019-03-01 00:00:00" AND "2019-03-31 23:59:59") AND tri.id_meta="'.$id.'" AND mf.id_trimestre = trim.id)as marzo'),

        'trim.tritrimestre as tritrimestre',
        'trim.tricausas_variacion as tricausas_variacion',
        'trim.trimedidas_correctivas as trimedidas_correctivas',
        'trim.triaccionb as triaccionb',
        'trim.tricantidad_rpogramada as cantidad_rpogramada',
        'trim.id as id_trimestre')
        ->leftjoin('memetas as met','met.id','=','trim.id_meta')
        ->where('met.id','=',$id)
        ->where('trim.tritrimestre', '=', 'Primer Trimestre')
        ->where('trim.id_meta','=',$id)
        ->get();
        return $meta_primer_trimestre;
    }

    public function returnDataSegundoTrimestre($id){
        $meta_segundo_trimestre = DB::table('metrimestres_metas as trim')
        ->select(DB::raw('(select mf.cantidad_programada_mes
        from metrimestres_metas as tri 
        left join memetas as me on me.id = tri.id_meta 
        left join metmetafiles as mf on tri.id = mf.id_trimestre
        where (mf.created_at BETWEEN "2019-04-01 00:00:00" AND "2019-04-30 23:59:59") AND tri.id_meta="'.$id.'" AND mf.id_trimestre = trim.id)as abril'),
        
        DB::raw('(select mf.cantidad_programada_mes
        from metrimestres_metas as tri 
        left join memetas as me on me.id = tri.id_meta 
        left join metmetafiles as mf on tri.id = mf.id_trimestre
        where (mf.created_at BETWEEN "2019-05-01 00:00:00" AND "2019-05-31 23:59:59") AND tri.id_meta="'.$id.'" AND mf.id_trimestre = trim.id)as mayo'),
        
        DB::raw('(select mf.cantidad_programada_mes
        from metrimestres_metas as tri 
        left join memetas as me on me.id = tri.id_meta 
        left join metmetafiles as mf on tri.id = mf.id_trimestre
        where (mf.created_at BETWEEN "2019-06-01 00:00:00" AND "2019-06-30 23:59:59") AND tri.id_meta="'.$id.'" AND mf.id_trimestre = trim.id)as junio'),

        'trim.tritrimestre as tritrimestre',
        'trim.tricausas_variacion as tricausas_variacion',
        'trim.trimedidas_correctivas as trimedidas_correctivas',
        'trim.triaccionb as triaccionb',
        'trim.tricantidad_rpogramada as cantidad_rpogramada',
        'trim.id as id_trimestre')
        ->leftjoin('memetas as met','met.id','=','trim.id_meta')
        ->where('met.id','=',$id)
        ->where('trim.tritrimestre', '=', 'Segundo Trimestre')
        ->where('trim.id_meta','=',$id)
        ->get();
        return $meta_segundo_trimestre;
    }

    public function returnDataTercerTrimestre($id){
        $meta_tercer_trimestre = DB::table('metrimestres_metas as trim')
        ->select(DB::raw('(select mf.cantidad_programada_mes
        from metrimestres_metas as tri 
        left join memetas as me on me.id = tri.id_meta 
        left join metmetafiles as mf on tri.id = mf.id_trimestre
        where (mf.created_at BETWEEN "2019-07-01 00:00:00" AND "2019-07-31 23:59:59") AND tri.id_meta="'.$id.'" AND mf.id_trimestre = trim.id)as julio'),
        
        DB::raw('(select mf.cantidad_programada_mes
        from metrimestres_metas as tri 
        left join memetas as me on me.id = tri.id_meta 
        left join metmetafiles as mf on tri.id = mf.id_trimestre
        where (mf.created_at BETWEEN "2019-08-01 00:00:00" AND "2019-08-31 23:59:59") AND tri.id_meta="'.$id.'" AND mf.id_trimestre = trim.id)as agosto'),
        
        DB::raw('(select mf.cantidad_programada_mes
        from metrimestres_metas as tri 
        left join memetas as me on me.id = tri.id_meta 
        left join metmetafiles as mf on tri.id = mf.id_trimestre
        where (mf.created_at BETWEEN "2019-09-01 00:00:00" AND "2019-09-30 23:59:59") AND tri.id_meta="'.$id.'" AND mf.id_trimestre = trim.id)as septiembre'),

        'trim.tritrimestre as tritrimestre',
        'trim.tricausas_variacion as tricausas_variacion',
        'trim.trimedidas_correctivas as trimedidas_correctivas',
        'trim.triaccionb as triaccionb',
        'trim.tricantidad_rpogramada as cantidad_rpogramada',
        'trim.id as id_trimestre')
        ->leftjoin('memetas as met','met.id','=','trim.id_meta')
        ->where('met.id','=',$id)
        ->where('trim.tritrimestre', '=', 'Tercer Trimestre')
        ->where('trim.id_meta','=',$id)
        ->get();
        return $meta_tercer_trimestre;
    }

    public function returnDataCuartoTrimestre($id){
        $meta_cuarto_trimestre = DB::table('metrimestres_metas as trim')
        ->select(DB::raw('(select mf.cantidad_programada_mes 
        from metrimestres_metas as tri 
        left join memetas as me on me.id = tri.id_meta 
        left join metmetafiles as mf on tri.id = mf.id_trimestre
        where (mf.created_at BETWEEN "2019-10-01 00:00:00" AND "2019-10-31 23:59:59") AND tri.id_meta="'.$id.'" AND mf.id_trimestre = trim.id)as octubre'),
        
        DB::raw('(select mf.cantidad_programada_mes
        from metrimestres_metas as tri 
        left join memetas as me on me.id = tri.id_meta 
        left join metmetafiles as mf on tri.id = mf.id_trimestre
        where (mf.created_at BETWEEN "2019-11-01 00:00:00" AND "2019-11-30 23:59:59") AND tri.id_meta="'.$id.'" AND mf.id_trimestre = trim.id)as noviembre'),
        
        DB::raw('(select mf.cantidad_programada_mes
        from metrimestres_metas as tri 
        left join memetas as me on me.id = tri.id_meta 
        left join metmetafiles as mf on tri.id = mf.id_trimestre
        where (mf.created_at BETWEEN "2019-12-01 00:00:00" AND "2019-12-31 23:59:59") AND tri.id_meta="'.$id.'" AND mf.id_trimestre = trim.id)as diciembre'),

        'trim.tritrimestre as tritrimestre',
        'trim.tricausas_variacion as tricausas_variacion',
        'trim.trimedidas_correctivas as trimedidas_correctivas',
        'trim.triaccionb as triaccionb',
        'trim.tricantidad_rpogramada as cantidad_rpogramada',
        'trim.id as id_trimestre')
        ->leftjoin('memetas as met','met.id','=','trim.id_meta')
        ->where('met.id','=',$id)
        ->where('trim.tritrimestre', '=', 'Cuarto Trimestre')
        ->where('trim.id_meta','=',$id)
        ->get();
        return $meta_cuarto_trimestre;
    }
    //totales anuales
    public function returnTotalesAnuales($id){
        $totales_anuales = DB::table('memetas as m')
        ->select(
            DB::raw('SUM(mt.tricantidad_rpogramada) as meta_anual'),
            DB::raw('(SELECT SUM(mf.cantidad_programada_mes)
            FROM memetas as m1
            LEFT OUTER JOIN metrimestres_metas as mt1 ON mt1.id_meta = m1.id
            LEFT OUTER JOIN metmetafiles as mf ON mf.id_trimestre = mt1.id
            WHERE mt1.id_meta = m.id
            ) as informe_acumulado'))
        ->leftJoin('metrimestres_metas as mt','mt.id_meta','=','m.id')
        ->where('m.id','=',$id)
        ->get();
        return $totales_anuales;
    }

    public function returnDataTrimestres(){
        $meta_trimestresallData = DB::table('metrimestres_metas as metri')
        ->select('metri.tritrimestre')
        ->leftjoin('memetas as met','met.id','=','metri.id_meta')
        ->get();
        return $meta_trimestresallData;
    }
    /*public function getTrimestres_data(){
        $return_trimestres = DB::table('memetas as met')
        
        ->select('metri.tritrimestre')
        ->leftjoin('metrimestres_metas as metri','metri.id_meta','=','met.id')
        ->get();
        
        return $return_trimestres();
    }*/
}
