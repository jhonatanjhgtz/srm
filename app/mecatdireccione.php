<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class mecatdireccione extends Model
{
    public function get_direcciones(){
        $direcciones = DB::table('mecatdirecciones')
        ->where('estatus','=','Activo')
        ->orderBy('nombre','ASC')
        ->get();
        foreach ($direcciones as $direccion) {
            $direccion->id = Crypt::encryptString($direccion->id);
            $direccion->nombre = $direccion->nombre;
        }
        return $direcciones;
    }
}
