<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

class memeta extends Model
{
    //Reporte general de metas
    //comenzamos sacando el query de la meta, proyecto, dirección, programa y unidad de medida 
    public function getmetas_data($direccion_user){
        //variable de id_direccion
        $valor = (Auth::user()->rol == 'Administrador')? '""' : $direccion_user;
        //variable de <> o = dependiendo de que rol es
        $operador_para_rol = (Auth::user()->rol == 'General')?"=":"<>";
        $return_metasdata = DB::table('memetas as met')
        ->select('met.id as id_meta',
        'met.memeta as meta',
        'met.menombre as nombre_de_la_accion',
        'dir.nombre as direccion', 
        DB::raw('CONCAT(pro.progprograma,"-",pro.prognombre) as programa'),
        DB::raw('CONCAT(pys.proproyecto,"-",pys.pronombre) as proyecto'),
        'met.memeta as meta', 
        'met.menombre as meta_nombre', 
        'uni.udnombre as unidad_medida')
        ->leftJoin('meproyectos as pys','pys.id','=','met.id_proyecto')
        ->leftJoin('mecatdirecciones as dir','dir.id','=','met.id_direccion')
        ->leftJoin('meprogramas as pro','pro.id','=','met.id_programa')
        ->leftJoin('mecatunidades as uni','uni.id','=','met.id_unidad_medida')
        ->where('met.id_direccion',$operador_para_rol,$valor)
        ->get();
        
        return $return_metasdata;
    }
    
    //obtener los trimestres que pertenecen a una meta
    public function returnDataTrimestres($id){
        $meta_trimestresallData = DB::table('metrimestres_metas as metri')
        ->select('metri.tritrimestre as trimestre',
                 'metri.id as id_trimestre')
        ->leftjoin('memetas as met','met.id','=','metri.id_meta')
        ->where('metri.id_meta','=',$id)
        ->get();
        return $meta_trimestresallData;
    }
}
