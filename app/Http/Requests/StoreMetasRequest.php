<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreMetasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'memeta' => 'required|unique:memetas,memeta',
            'menombre' => 'required',
            'id_unidad_medida' => 'required',
            'id_direccion' => 'required',
            'id_programa' => 'required',
            'id_proyecto' => 'required'
        ];
    }
}
