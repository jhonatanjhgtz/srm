<?php

namespace App\Http\Controllers;

use App\mecatdireccione;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreDireccionRequest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

class direccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $data = new mecatdireccione();
        $direcciones = $data->get_direcciones();
        return view('direcciones.index',compact('direcciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        return view('direcciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDireccionRequest $request)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $direccion = new mecatdireccione();
        $direccion->nombre = $request->input('nombre');
        $direccion->save();
        return redirect()->route('direcciones.index')->with('status','Dirección Registrada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $direccion = mecatdireccione::where('id',Crypt::decryptString($id))->firstOrFail();
        return view('direcciones.edit',compact('direccion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $direccion = mecatdireccione::where('id',$id)->firstOrFail();
        $direccion->nombre = $request->input('nombre');
        $direccion->save();
        return redirect()->route('direcciones.index')->with('status','Información actualizada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
