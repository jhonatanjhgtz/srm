<?php

namespace App\Http\Controllers;

use App\meprograma;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreProgramasRequest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

class programaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $data = new meprograma();
        $programas = $data->get_programas();
        return view('programas.index',compact('programas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        return view('programas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProgramasRequest $request)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $programa = new meprograma();
        $programa->progprograma = $request->input('progprograma');
        $programa->prognombre = $request->input('prognombre'); 
        $programa->save();
        return redirect()->route('programas.index')->with('status','Programa registrado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $programa = meprograma::where('id',Crypt::decryptString($id))->firstOrFail();
        return view('programas.edit',compact('programa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $programa = meprograma::where('id',$id)->firstOrFail();
        $programa->progprograma = $request->input('progprograma');
        $programa->prognombre = $request->input('prognombre');
        $programa->save();
        return redirect()->route('programas.index')->with('status','Información actualizada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
