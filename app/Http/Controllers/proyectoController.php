<?php

namespace App\Http\Controllers;

use App\meproyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreProyectoRequest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

class proyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $data = new meproyecto();
        $proyectos = $data->get_proyectos();
        return view('proyectos.index',compact('proyectos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        return view('proyectos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProyectoRequest $request)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $proyecto = new meproyecto();
        $proyecto->proproyecto = $request->input('proproyecto');
        $proyecto->pronombre = $request->input('pronombre'); 
        $proyecto->save();
        return redirect()->route('proyectos.index')->with('status','Proyecto registrado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $proyecto = meproyecto::where('id',Crypt::decryptString($id))->firstOrFail();
        return view('proyectos.edit',compact('proyecto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $proyecto = meproyecto::where('id',$id)->firstOrFail();
        $proyecto->proproyecto = $request->input('proproyecto');
        $proyecto->pronombre = $request->input('pronombre');
        $proyecto->save();
        return redirect()->route('proyectos.index')->with('status','Información actualizada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
