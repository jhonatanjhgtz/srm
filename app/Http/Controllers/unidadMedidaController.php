<?php

namespace App\Http\Controllers;

use App\mecatunidade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreUnidadesRequest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

class unidadMedidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $data = new mecatunidade();
        $unidades = $data->get_unidades();
        return view('unidades.index',compact('unidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        return view('unidades.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUnidadesRequest $request)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $unidad = new mecatunidade();
        $unidad->udnombre = $request->input('udnombre');
        $unidad->save();
        return redirect()->route('unidades_medida.index')->with('status','Unidad registrada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $unidad = mecatunidade::where('id',Crypt::decryptString($id))->firstOrFail();
        return view('unidades.edit',compact('unidad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUnidadesRequest $request, $id)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $unidad = mecatunidade::where('id',$id)->firstOrFail();
        $unidad->udnombre = $request->input('udnombre');
        $unidad->save();
        return redirect()->route('unidades_medida.index')->with('status','Información actualizada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
