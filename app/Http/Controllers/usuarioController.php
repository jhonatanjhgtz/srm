<?php

namespace App\Http\Controllers;

use App\User;
use App\mecatdireccione;
use App\metmetafile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use App\Http\Requests\StoreUsuariosRequest;
use App\Http\Requests\StoreUsuariosUpdateRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class usuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $data = new User();
        $usuarios = $data->get_usuarios();
        return view('usuarios.index',compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $direcciones = mecatdireccione::pluck('nombre','id');
        return view('usuarios.create',compact('direcciones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUsuariosRequest $request)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $usuario = new User();
        $usuario->name = $request->input('name');
        $usuario->email = $request->input('email');
        $usuario->rol = $request->input('rol');
        $usuario->id_direccion = $request->input('id_direccion');
        $usuario->usuario_registro = Auth::user()->id;
        $usuario->save();
        return redirect()->route('usuarios.index')->with('status','Usuario Registrado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $direcciones = mecatdireccione::pluck('nombre','id');
        $usuarios = User::where('id',$id)->firstOrFail();
        return view('usuarios.edit',compact('usuarios','direcciones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUsuariosUpdateRequest $request, $id)
    {
        if(Auth::user()->rol == 'General'){
            return redirect()->route('home')->with('message','¡Acceso restringido!');
        }
        $usuario = User::where('id',$id)->firstOrFail();
        $usuario->name = $request->input('name');
        $usuario->email = $request->input('email');
        $usuario->rol = $request->input('rol');
        $usuario->id_direccion = $request->input('id_direccion');
        $usuario->usuario_modifico = Auth::user()->id;
        $usuario->save();
        return redirect()->route('usuarios.index')->with('status','Usuario Modificado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
