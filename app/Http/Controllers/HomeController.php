<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\memeta;
use App\metrimestres_meta;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = new memeta();
        $dataT = new metrimestres_meta();
        $metas = $data->getmetas_data(Auth::user()->id_direccion);
        $estructura_reporte='<div class="table-responsive card-box" data-pattern="priority-columns"><table border=1>';
        //thead
        $celda ="";
        $celda2 ="";
        $celda3 ="";
        //tbody
        $celda4 ="";

        $cuenta=0;
        
        foreach ($metas as $meta) {
            $id = $meta->id_meta;
            $infoAnual = $dataT->returnTotalesAnuales($id);
            $avance="";
            if($infoAnual[0]->informe_acumulado !="" && $infoAnual[0]->meta_anual !=""){
                $avance = $infoAnual[0]->informe_acumulado/$infoAnual[0]->meta_anual*100;
            }
            $desempeno="";
            if($avance<49.9){
                $desempeno="rojo";
            }elseif($avance>50.1 && $avance<69.9){
                $desempeno="naranja";
            }elseif($avance>70 && $avance<89.9){
                $desempeno="amarilla";
            }elseif($avance>90 && $avance<110){
                $desempeno="verde";
            }else{
                $desempeno="morada";
            }
            //cabecera principal
            $estructura_reporte.='
                    <tr class="bg-success text-white">
                        <th colspan="6" class="text-center">Anual / Programa: "'.$meta->programa.'" / Proyecto: "'.$meta->proyecto.'"</th>
                    </tr>
                    <tr>
                        <th>Meta</th>
                        <th>Nombre de la acción</th>
                        <th>Unidad de medida</th>
                        <th>Meta anual</th>
                        <th>Informe acumulado</th>
                        <th>Avance</th>
                    </tr>
                    <tr>
                        <td>'.$meta->meta.'</td>
                        <td>'.$meta->nombre_de_la_accion.'</td>
                        <td>'.$meta->unidad_medida.'</td>
                        <td>'.$infoAnual[0]->meta_anual.'</td>
                        <td>'.$infoAnual[0]->informe_acumulado.'</td>
                        <td class="'.$desempeno.'">'.number_format((float)$avance, 1, '.', '').'%</td>
                    </tr>';
            //realizados por los tres meses correspondinte a los trimestres
            $realizadoPrimerTrimestre = $dataT->returnDataPrimerTrimestre($id);
            $realizadoSegundoTrimestre = $dataT->returnDataSegundoTrimestre($id);
            $realizadoTercerTrimestre = $dataT->returnDataTercerTrimestre($id);
            $realizadoCuartoTrimestre = $dataT->returnDataCuartoTrimestre($id);
            //recorrido de los cuatrimestres
            foreach ($realizadoPrimerTrimestre as $primertri) {
                //primer trimestre
                $cuenta=$cuenta+1;
                $estructura_reporte.='<thead>';
                //<tr class="bg-success text-white">
                    $celda .='<th colspan="9" class="text-center">'.$primertri->tritrimestre.'</th>';
                //</tr>
                //<tr>
                    $celda2 .='<th rowspan="2" class="text-center">Cantidad programada</th>
                               <th colspan="5" class="text-center">Realizado</th>
                               <th colspan="2" class="text-center">Justificación trimestral en caso de ubicarse en las zonas: roja, amarilla, naranja o morada  (A)</th>
                               <th rowspan="2" class="text-center">Descripción de la acción (B)</th>';
                    //<tr class="text-center">
                        $celda3.='<td>enero</td>
                                  <td>febrero</td>
                                  <td>marzo</td>
                                  <td>Acumulado trimestral</td><td>Avance trimestral</td><td>Causas de la variación</td>
                                  <td>Medidas Correctivas</td>';
                    //</tr>
                //</tr>
                $estructura_reporte.='</thead>';
                $estructura_reporte.='<tbody>';
                
                $primerTrimestreMeses ="";
                $primerTrimestreMeses.='<td align="center">'.$primertri->enero.'</td>';
                $primerTrimestreMeses.='<td align="center">'.$primertri->febrero.'</td>';
                $primerTrimestreMeses.='<td align="center">'.$primertri->marzo.'</td>';
                
                    $acumulado_trimestralP = ($primertri->enero + $primertri->febrero + $primertri->marzo);    
                    $avancetrimestralP="";
                    if($acumulado_trimestralP !=0){
                        $avancetrimestralP.=number_format((float)$acumulado_trimestralP/$primertri->cantidad_rpogramada*100, 1, '.', '');
                    } 

                    $celda4.='<td align="center">'.$primertri->cantidad_rpogramada.'</td><!--Cantidad programada-->'
                    .$primerTrimestreMeses.'
                    <td align="center">'.$acumulado_trimestralP.'</td><!--AcumuladoTrimestral-->
                    <td align="center">'.$avancetrimestralP.'%</td><!--AvanceTrimestral-->
                    <td></td><!--CausasVariación-->
                    <td></td><!--MedidasCorrectivas-->
                    <td></td><!--DescripciónAcciónB-->';

                $estructura_reporte.='</tbody>';

                if ($cuenta>3){
                    $estructura_reporte.='<tr class="bg-success text-white">'.$celda.'</tr>';
                    $estructura_reporte.='<tr>'.$celda2.'<tr>'.$celda3.'</tr></tr>';
                    $estructura_reporte.='<tr>'.$celda4.'</tr>';
                    $celda = "";
                    $celda2 = "";
                    $celda3 = "";
                    $celda4 = "";
                    $cuenta= 0;
                }
            }
            foreach ($realizadoSegundoTrimestre as $segundotri) {
                //segundo cuatrimestre
                $cuenta=$cuenta+1;
                $estructura_reporte.='<thead>';
                //<tr class="bg-success text-white">
                    $celda .='<th colspan="9" class="text-center">'.$segundotri->tritrimestre.'</th>';
                //</tr>
                //<tr>
                    $celda2 .='<th rowspan="2" class="text-center">Cantidad programada</th>
                               <th colspan="5" class="text-center">Realizado</th>
                               <th colspan="2" class="text-center">Justificación trimestral en caso de ubicarse en las zonas: roja, amarilla, naranja o morada  (A)</th>
                               <th rowspan="2" class="text-center">Descripción de la acción (B)</th>';
                    //<tr class="text-center">
                        $celda3.='<td>abril</td>
                                  <td>mayo</td>
                                  <td>junio</td>
                                  <td>Acumulado trimestral</td><td>Avance trimestral</td><td>Causas de la variación</td>
                                  <td>Medidas Correctivas</td>';
                    //</tr>
                //</tr>
                $estructura_reporte.='</thead>';
                $estructura_reporte.='<tbody>';
                
                $segundoTrimestreMeses ="";
                $segundoTrimestreMeses.='<td align="center">'.$segundotri->abril.'</td>';
                $segundoTrimestreMeses.='<td align="center">'.$segundotri->mayo.'</td>';
                $segundoTrimestreMeses.='<td align="center">'.$segundotri->junio.'</td>';

                    $acumulado_trimestralS = ($segundotri->abril + $segundotri->mayo + $segundotri->junio);
                    $avancetrimestralS="";
                    if($acumulado_trimestralS !=0){
                        $avancetrimestralS.=number_format((float)$acumulado_trimestralS/$segundotri->cantidad_rpogramada*100, 1, '.', '');
                    }

                    $celda4.='<td align="center">'.$segundotri->cantidad_rpogramada.'</td><!--Cantidad programada-->'
                    .$segundoTrimestreMeses.'
                    <td align="center">'.$acumulado_trimestralS.'</td><!--AcumuladoTrimestral-->
                    <td align="center">'.$avancetrimestralS.'%</td><!--AvanceTrimestral-->
                    <td></td><!--CausasVariación-->
                    <td></td><!--MedidasCorrectivas-->
                    <td></td><!--DescripciónAcciónB-->';

                $estructura_reporte.='</tbody>';

                if ($cuenta>3){
                    $estructura_reporte.='<tr class="bg-success text-white">'.$celda.'</tr>';
                    $estructura_reporte.='<tr>'.$celda2.'<tr>'.$celda3.'</tr></tr>';
                    $estructura_reporte.='<tr>'.$celda4.'</tr>';
                    $celda = "";
                    $celda2 = "";
                    $celda3 = "";
                    $celda4 = "";
                    $cuenta= 0;
                }
            }
            foreach ($realizadoTercerTrimestre as $tercertri) {
                //tercer trimestre
                $cuenta=$cuenta+1;
                $estructura_reporte.='<thead>';
                //<tr class="bg-success text-white">
                    $celda .='<th colspan="9" class="text-center">'.$tercertri->tritrimestre.'</th>';
                //</tr>
                //<tr>
                    $celda2 .='<th rowspan="2" class="text-center">Cantidad programada</th>
                               <th colspan="5" class="text-center">Realizado</th>
                               <th colspan="2" class="text-center">Justificación trimestral en caso de ubicarse en las zonas: roja, amarilla, naranja o morada  (A)</th>
                               <th rowspan="2" class="text-center">Descripción de la acción (B)</th>';
                    //<tr class="text-center">
                        $celda3.='<td>julio</td>
                                  <td>agosto</td>
                                  <td>septiembre</td>
                                  <td>Acumulado trimestral</td><td>Avance trimestral</td><td>Causas de la variación</td>
                                  <td>Medidas Correctivas</td>';
                    //</tr>
                //</tr>
                $estructura_reporte.='</thead>';
                $estructura_reporte.='<tbody>';
                
                $tercerTrimestreMeses ="";
                $tercerTrimestreMeses.='<td align="center">'.$tercertri->julio.'</td>';
                $tercerTrimestreMeses.='<td align="center">'.$tercertri->agosto.'</td>';
                $tercerTrimestreMeses.='<td align="center">'.$tercertri->septiembre.'</td>';
                
                    $acumualdotrimestralT = ($tercertri->julio + $tercertri->agosto + $tercertri->septiembre);
                    $avancetrimestralT="";
                    if($avancetrimestralT !=0){
                        $avancetrimestralT.= number_format((float)$acumualdotrimestralT/$tercertri->cantidad_rpogramada*100, 1, '.', '');
                    }

                    $celda4.='<td align="center">'.$tercertri->cantidad_rpogramada.'</td><!--Cantidad programada-->'
                    .$tercerTrimestreMeses.'
                    <td align="center">'.$acumualdotrimestralT.'</td><!--AcumuladoTrimestral-->
                    <td align="center">'.$avancetrimestralT.'%</td><!--AvanceTrimestral-->
                    <td></td><!--CausasVariación-->
                    <td></td><!--MedidasCorrectivas-->
                    <td></td><!--DescripciónAcciónB-->';

                $estructura_reporte.='</tbody>';

                if ($cuenta>3){
                    $estructura_reporte.='<tr class="bg-success text-white">'.$celda.'</tr>';
                    $estructura_reporte.='<tr>'.$celda2.'<tr>'.$celda3.'</tr></tr>';
                    $estructura_reporte.='<tr>'.$celda4.'</tr>';
                    $celda = "";
                    $celda2 = "";
                    $celda3 = "";
                    $celda4 = "";
                    $cuenta= 0;
                }
            }
            foreach ($realizadoCuartoTrimestre as $cuartotri) {
                //Cuarto trimestre
                $cuenta=$cuenta+1;
                $estructura_reporte.='<thead>';
                //<tr class="bg-success text-white">
                    $celda .='<th colspan="9" class="text-center">'.$cuartotri->tritrimestre.'</th>';
                //</tr>
                //<tr>
                    $celda2 .='<th rowspan="2" class="text-center">Cantidad programada</th>
                            <th colspan="5" class="text-center">Realizado</th>
                            <th colspan="2" class="text-center">Justificación trimestral en caso de ubicarse en las zonas: roja, amarilla, naranja o morada  (A)</th>
                            <th rowspan="2" class="text-center">Descripción de la acción (B)</th>';
                    //<tr class="text-center">
                        $celda3.='<td>octubre</td>
                                <td>noviembre</td>
                                <td>diciembre</td>
                                <td>Acumulado trimestral</td><td>Avance trimestral</td><td>Causas de la variación</td>
                                <td>Medidas Correctivas</td>';
                    //</tr>
                //</tr>
                $estructura_reporte.='</thead>';
                $estructura_reporte.='<tbody>';

                $cuartoTrimestreMeses ="";
                $cuartoTrimestreMeses.='<td align="center">'.$cuartotri->octubre.'</td>';
                $cuartoTrimestreMeses.='<td align="center">'.$cuartotri->noviembre.'</td>';
                $cuartoTrimestreMeses.='<td align="center">'.$cuartotri->diciembre.'</td>';
                    
                    $acumualdotrimestralC = ($cuartotri->octubre + $cuartotri->noviembre + $cuartotri->diciembre);
                    $avancetrimestralC="";
                    if($acumualdotrimestralC !=0){
                        $avancetrimestralC.= number_format((float)$acumualdotrimestralC/$cuartotri->cantidad_rpogramada*100, 1, '.', '');
                    }


                    $celda4.='<td align="center">'.$cuartotri->cantidad_rpogramada.'</td><!--Cantidad programada-->'
                    .$cuartoTrimestreMeses.'
                    <td align="center">'.$acumualdotrimestralC.'</td><!--AcumuladoTrimestral-->
                    <td align="center">'.$avancetrimestralC.'%</td><!--AvanceTrimestral-->
                    <td></td><!--CausasVariación-->
                    <td></td><!--MedidasCorrectivas-->
                    <td></td><!--DescripciónAcciónB-->';

                $estructura_reporte.='</tbody>';

                if ($cuenta>3){
                    $estructura_reporte.='<tr class="bg-success text-white">'.$celda.'</tr>';
                    $estructura_reporte.='<tr>'.$celda2.'<tr>'.$celda3.'</tr></tr>';
                    $estructura_reporte.='<tr>'.$celda4.'</tr>';
                    $celda = "";
                    $celda2 = "";
                    $celda3 = "";
                    $celda4 = "";
                    $cuenta= 0;
                }
            }

            /*$trimestres = $data->returnDataTrimestres($id);
            foreach ($trimestres as $key => $trimestre) {
               
            }*/
        }
        $estructura_reporte.='</table></div>';
        return view('home',compact('estructura_reporte'));
    }
}
/*foreach ($metas as $meta) {
                $id = $meta->id_meta;
                //realizados por los tres meses correspondinte a los trimestres
                $realizadoPrimerTrimestre = $dataT->returnDataPrimerTrimestre($id);
                $realizadoSegundoTrimestre = $dataT->returnDataSegundoTrimestre($id);
                $realizadoTercerTrimestre = $dataT->returnDataTercerTrimestre($id);
                $realizadoCuartoTrimestre = $dataT->returnDataCuartoTrimestre($id);
                foreach ($realizadoPrimerTrimestre as $primertri) {
                    echo"<ul>".$primertri->tritrimestre."</ul>";
                    echo"<li>".$primertri->enero."-";
                    echo$primertri->febrero."-";
                    echo$primertri->marzo."</li><br>";
                }
                foreach ($realizadoSegundoTrimestre as $segundotri) {
                    echo"<ul>".$segundotri->tritrimestre."</ul>";
                    echo"<li>".$segundotri->abril."-";
                    echo$segundotri->mayo."-";
                    echo$segundotri->junio."</li><br>";
                }
                foreach ($realizadoTercerTrimestre as $tercertri) {
                    echo"<ul>".$tercertri->tritrimestre."</ul>";
                    echo"<li>".$tercertri->julio."-";
                    echo$tercertri->agosto."-";
                    echo$tercertri->septiembre."</li><br>";
                }
                foreach ($realizadoCuartoTrimestre as $cuartotri) {
                    echo"<ul>".$cuartotri->tritrimestre."</ul>";
                    echo"<li>".$cuartotri->octubre."-";
                    echo$cuartotri->noviembre."-";
                    echo$cuartotri->diciembre."</li>";
                }
        }*/