<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\metrimestres_meta;
use App\metmetafile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;

class trimestreArchivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cantidadProgramada = metmetafile::where('id', $id)->firstOrFail();
        return view('trimestres.editarCantidadProgramada',compact('cantidadProgramada'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $archivoTrimestreupdate = metmetafile::where('id',$id)->firstOrFail();
        if($request->hasFile('archnombre_')){
            //eliminamos el archivo
            $id_trimestre = $request->input('id_trimestre');
            $archivoTrimestre = metmetafile::where('archnombre',$request->input('archnombre'))->firstOrFail();
            $file_path = public_path().'/files/'.$request->input('archnombre');
            \File::delete($file_path);
            //agregarmos el archivo
            $file = $request->file('archnombre_');
            //Nombre de archivo
            $name = time().$file->getClientOriginalName();
            //ruta de almacenamiento
            $file->move(public_path().'/files/', $name);
            //
            $archivoTrimestreupdate->archnombre = $name;
        }
        
        $archivoTrimestreupdate->cantidad_programada_mes = $request->input('cantidad_programada_mes');
        $archivoTrimestreupdate->save();
        return redirect()->route('metas.index')->with('status','Información actualizada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
