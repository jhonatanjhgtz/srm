<?php

namespace App\Http\Controllers;

use App\memeta;
use App\mecatunidade;
use App\mecatdireccione;
use App\meprograma;
use App\meproyecto;
use App\metrimestres_meta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreMetasRequest;
use App\Http\Requests\StoreMetaUpdateRequest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class metaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
        $data = new metrimestres_meta();
        $infometas = $data->returnDataMetas('ver_metas',Auth::user()->id_direccion,Auth::user()->rol);
        return view('metas.index',compact('infometas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->middleware('auth');
        $unidades = mecatunidade::all(); $direcciones = mecatdireccione::all(); $programas = meprograma::all(); $proyectos = meproyecto::all();

        return view('metas.create',compact('unidades','direcciones','programas','proyectos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMetasRequest $request)
    {
        $this->middleware('auth');
        $meta = new memeta();
        $meta->memeta = $request->input('memeta');
        $meta->menombre = $request->input('menombre');
        $meta->id_unidad_medida = $request->input('id_unidad_medida');
        $meta->id_direccion = $request->input('id_direccion');
        $meta->id_programa = $request->input('id_programa');
        $meta->id_proyecto = $request->input('id_proyecto');
        $meta->save();
        /*
        Retornar el id de la meta registrada para comenzar a insertar los trimestres 
        correspondientes a la misma registrada
        */
        
        //COMENTADO
        //$id_meta = $meta->id;
        //return redirect()->route('trimestres.edit',$id_meta);
        
        
        //REGISTRO DE TRIMESTRE "PRIMER FUNCIONAMIENTO DE REGISTRO"
        $id_meta = $meta->id;
        $idmeta = array(0=>$id_meta,1=>$id_meta,2=>$id_meta,3=>$id_meta);
        $trimestre = array(0=>'Primer Trimestre',1=>'Segundo Trimestre',2=>'Tercer Trimestre',3=>'Cuarto Trimestre');
        for($i=0;$i<4;$i++){
            DB::table('metrimestres_metas')->insertGetId(
                ['tritrimestre' =>$trimestre[$i],'id_meta' => $idmeta[$i],'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
            );
        }
        //return redirect()->route('trimestres.edit',$trimestre->id)->with('status','Primer trimestre registrado.');
        return redirect()->route('metas.index')->with('status','Meta registrada.');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->middleware('auth');
        $data = new metrimestres_meta();
        $infometa = $data->returnDataMeta($id);
        $infoPrimerTrimestre  = $data->returnDataPrimerTrimestre($id);
        $infoSegundoTrimestre = $data->returnDataSegundoTrimestre($id);
        $infoTercerTrimestre  = $data->returnDataTercerTrimestre($id);
        $infoCuartoTrimestre  = $data->returnDataCuartoTrimestre($id);
        $infoAnual = $data->returnTotalesAnuales($id);
        $avance="";
        if($infoAnual[0]->informe_acumulado !="" && $infoAnual[0]->meta_anual !=""){
            $avance = $infoAnual[0]->informe_acumulado/$infoAnual[0]->meta_anual*100;
        }
        $desempeno="";
        if($avance<49.9){
            $desempeno="rojo";
        }elseif($avance>50.1 && $avance<69.9){
            $desempeno="naranja";
        }elseif($avance>70 && $avance<89.9){
            $desempeno="amarilla";
        }elseif($avance>90 && $avance<110){
            $desempeno="verde";
        }else{
            $desempeno="morada";
        }
        return view('metas.show',compact('infometa','infoPrimerTrimestre','infoSegundoTrimestre','infoTercerTrimestre','infoCuartoTrimestre','infoAnual','avance','desempeno'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meta = memeta::where('id',$id)->firstOrFail();
        $unidades = mecatunidade::all(); $direcciones = mecatdireccione::all(); $programas = meprograma::all(); $proyectos = meproyecto::all();
        return view('metas.edit',compact('meta','unidades','direcciones','programas','proyectos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMetaUpdateRequest $request, $id)
    {
        $meta = memeta::where('id',$id)->firstOrFail();
        $meta->memeta = $request->input('memeta');
        $meta->menombre = $request->input('menombre');
        $meta->id_unidad_medida = $request->input('id_unidad_medida');
        $meta->id_direccion = $request->input('id_direccion');
        $meta->id_programa = $request->input('id_programa');
        $meta->id_proyecto = $request->input('id_proyecto');
        $meta->save();
        return redirect()->route('metas.index')->with('status','Meta Modificada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
