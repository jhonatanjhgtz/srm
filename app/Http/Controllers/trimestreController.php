<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\metrimestres_meta;
use App\metmetafile;
use App\metrimestre_reconduccione;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;

class trimestreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_trimestre)
    {
        $data = new metrimestres_meta();
        $meta_allData = $data->returnDataMetaTrimestre('update_trimestre',$id_trimestre);
        $trimestre = metrimestres_meta::where('id',$id)->firstOrFail();
        $archivos_trimestre = metmetafile::all()->where('id_trimestre',$id_trimestre);
        return view('trimestres.create',compact('trimestre','meta_allData','archivos_trimestre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id_trimestre)
    {
        echo"Si llego el id del trimestre: ".$id_trimestre;
        die();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //id_meta
    public function edit($id)
    {
        $data = new metrimestres_meta();
        $meta_allData = $data->returnDataMetaTrimestre($id);
        $trimestre = metrimestres_meta::where('id',$id)->firstOrFail();
        $archivos_trimestre = metmetafile::all()->where('id_trimestre',$id);
        return view('trimestres.edit',compact('trimestre','meta_allData','archivos_trimestre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $trimestre = metrimestres_meta::where('id',$id)->firstOrFail();
        if($request->hasFile('trirereconduccion') && $request->input('trireconduccion_skip') == 'Si'){
            $archivoTrimestreReconduccion = new metrimestre_reconduccione();
                $file = $request->file('trirereconduccion');
                //Nombre de archivo
                $name = time().$file->getClientOriginalName();
                //ruta de almacenamiento
                $file->move(public_path().'/files/', $name);
                //traer la cantidad programada anterior
                $archivoTrimestreReconduccion->trirecantidad_programada_anterior = $request->input('cantidad_programada_mes_skip');
                $archivoTrimestreReconduccion->trirereconduccion = $name;
                $archivoTrimestreReconduccion->id_metrimestres = $id; 
                $archivoTrimestreReconduccion->save();
                
        }
        if($request->input('trireconduccion')!=""){
            $trimestre->trireconduccion = $request->input('trireconduccion_skip');
        }
        $trimestre->tricantidad_rpogramada = $request->input('tricantidad_rpogramada');
        $trimestre->tricausas_variacion = $request->input('tricausas_variacion');
        $trimestre->trimedidas_correctivas = $request->input('trimedidas_correctivas');
        $trimestre->triaccionb = $request->input('triaccionb');
        $trimestre->save();
        return $this->edit($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadFile(Request $request)
    {   
        //validar si es unaolictud ajax
        if($request->ajax()){
            if($request->hasFile('archnombre')){
                $id = $request->input('id_trimestre');
                $archivoTrimestre = new metmetafile();
                $file = $request->file('archnombre');
                //Nombre de archivo
                $name = time().$file->getClientOriginalName();
                //ruta de almacenamiento
                $file->move(public_path().'/files/', $name);
                
                $archivoTrimestre->archnombre = $name;
                $archivoTrimestre->id_trimestre = $request->input('id_trimestre');
                $archivoTrimestre->cantidad_programada_mes = $request->input('cantidad_programada_mes');
                $archivoTrimestre->save();
                $data = $this->getFilesIdTrimestre($id);
                $archivos_trimestre = $data;
                return response()->view('trimestres.listadoarchivos',compact('archivos_trimestre'));
                /*return response()->json([
                    $archivos_trimestre,
                ]);*/
            }
        }
    }

    public function deleteFileTirmeste(Request $request, $id){
        //validar porque no llega el ID
        if($request->ajax()){
            $id_trimestre = $request->input('id_trimestre');
            $archivoTrimestre = metmetafile::where('archnombre',$request->input('archnombre'))->firstOrFail();
            $file_path = public_path().'/files/'.$request->input('archnombre');
            \File::delete($file_path);
            $archivoTrimestre->delete();
            $data = $this->getFilesIdTrimestre($id_trimestre);
            $archivos_trimestre = $data;
            return response()->view('trimestres.listadoarchivos',compact('archivos_trimestre'));
        }
    }

    public function getFilesIdTrimestre($id)
    {
        $trimestre_archivos = metmetafile::all()->where('id_trimestre',$id);
        /*$dataHTML="";
        foreach ($trimestre_archivos as $listado_archivo) {
            $dataHTML.="{!! Form::open(['route' => ['deleteFile', $listado_archivo->id], 'method' => 'DELETE']) !!}";
            $dataHTML.="<a href='{{asset('files/$listado_archivo->archnombre')}}' target='_blank' class='feleteFile'><i class='mdi mdi-file text-success' title='Ver archivo'></i></a><a href='#' class='feleteFile text-danger' title='Eliminar archivo'><i class='mdi mdi-delete-forever'></i></a>";
            $dataHTML.="{!! Form::close() !!}";
        }*/
        return $trimestre_archivos;
    }
}
