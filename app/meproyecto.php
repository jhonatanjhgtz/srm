<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class meproyecto extends Model
{
    public function get_proyectos(){
        $proyectos = DB::table('meproyectos')
        ->where('proestatus','=','Activo')
        ->orderby('id','DESC')
        ->get();
        foreach ($proyectos as $proyecto) {
            $proyecto->id =Crypt::encryptString($proyecto->id);
        }
        return $proyectos;
    }
}
