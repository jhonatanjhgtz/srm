@extends('layouts.app')

@section('title','Inicio')

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Bienvenido {{ Auth::user()->name }}</h4>
                        <ol class="breadcrumb float-right">
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1>Reporte de metas</h1>
                    @if(session()->has('message'))
                        <div class="alert alert-warning">{{ session('message') }}</div>
                    @endif
                    {!!$estructura_reporte!!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
