@extends('layouts.app')

@section('title','Crear Programa')
@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0"></ol>
                        </div>
                        <h4 class="page-title">Registro de Programa</h4>
                        <div class="clearfix"></div>
                        <a href="{{ route('programas.index') }}"><i class="mdi mdi-keyboard-return"></i> Regresar</a>
                    </div>
                </div>
            </div>
            @include('common.errors')
            <div class="row">
                <div class="col-md-6 offset-md-3 m-t-20">
                    <div class="card-box">
                    {!! Form::open(['route' => 'programas.store', 'method' => 'POST','class' => 'form-group']) !!}
                    @include('programas.form')
                    {!! Form::submit('Guardar',['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection