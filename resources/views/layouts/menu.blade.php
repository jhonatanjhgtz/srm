<!-- Top Bar Start -->
<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            <a href="#" class="logo"><i class="ti"></i> <span>Reporte de Metas</span></a>
        </div>
    </div>

    <!-- Button mobile view to collapse sidebar menu -->
    <nav class="navbar-custom">

        <ul class="list-inline float-right mb-0">
                <img src="{{ asset('/images/logo_login.png') }}" alt="" width="320px">
            <!--<li class="list-inline-item dropdown notification-list">
                <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                    aria-haspopup="false" aria-expanded="false">
                    <i class="mdi mdi-bell noti-icon"></i>
                    <span class="badge badge-pink noti-icon-badge">0</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg" aria-labelledby="Preview">
                    
                    <div class="dropdown-item noti-title">
                        <h5 class="font-16"><span class="badge badge-danger float-right">5</span>Notificaciones</h5>
                    </div>

                    
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-success"><i class="mdi mdi-comment-account"></i></div>
                        <p class="notify-details">Robert S. Taylor commented on Admin<small class="text-muted">1 min ago</small></p>
                    </a>

                    
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-info"><i class="mdi mdi-account"></i></div>
                        <p class="notify-details">New user registered.<small class="text-muted">1 min ago</small></p>
                    </a>

                    
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <div class="notify-icon bg-danger"><i class="mdi mdi-airplane"></i></div>
                        <p class="notify-details">Carlos Crouch liked <b>Admin</b><small class="text-muted">1 min ago</small></p>
                    </a>

                    
                    <a href="javascript:void(0);" class="dropdown-item notify-item notify-all">
                        View All
                    </a>

                </div>
            </li>-->

            <li class="list-inline-item dropdown notification-list">
                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                    aria-haspopup="false" aria-expanded="false">
                    <i class="mdi mdi-account-circle"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                    <!-- item-->
                    <a class="dropdown-item notify-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                        <i class="mdi mdi-logout"></i>
                        {{ __('Salir') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>

        </ul>

        <ul class="list-inline menu-left mb-0">
            <li class="float-left">
                <button class="button-menu-mobile open-left waves-light waves-effect">
                    <i class="mdi mdi-menu"></i>
                </button>
            </li>
        </ul>

    </nav>

</div>
<!-- Top Bar End -->

<!-- ========== Left Sidebar Start ========== -->

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li class="menu-title">Main</li>

                <li>
                    <a href="{{ route('home') }}" class="waves-effect waves-primary"><i class="ti-home"></i><span> Inicio </span></a>
                </li>
                @if(Auth::user()->rol == 'Administrador')
                    <li class="has_sub">
                        <a href="{{ route('direcciones.index')}}" class="waves-effect"><i class="ti-agenda"></i><span>Direcciones</span> <span class="menu-arrow"></span></a>
                    </li>
                @endif
                @if(Auth::user()->rol == 'Administrador')
                <li class="has_sub">
                    <a href="{{ route('unidades_medida.index') }}" class="waves-effect"><i class="ti-list-ol"></i><span>Unidad de medida</span> <span class="menu-arrow"></span></a>
                </li>
                @endif
                @if(Auth::user()->rol == 'Administrador')
                <li class="has_sub">
                    <a href="{{ route('programas.index') }}" class="waves-effect"><i class="ti-folder"></i><span>Programas</span> <span class="menu-arrow"></span></a>
                </li>
                @endif
                @if(Auth::user()->rol == 'Administrador')
                <li class="has_sub">
                    <a href="{{ route('proyectos.index') }}" class="waves-effect"><i class="ti-agenda"></i><span>Proyectos</span> <span class="menu-arrow"></span></a>
                </li>
                @endif
                <li class="has_sub">
                    <a href="{{ route('metas.index') }}" class="waves-effect"><i class="ti-bar-chart"></i><span>Metas</span> <span class="menu-arrow"></span></a>
                </li>
                @if(Auth::user()->rol == 'Administrador')
                <li class="has_sub">
                    <a href="{{ route('usuarios.index') }}" class="waves-effect"><i class="ti-user"></i><span>Usuarios</span></a>
                </li>
                @endif
            </ul>

            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->