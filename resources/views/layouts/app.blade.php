<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte de Metas - @yield('title')</title>
    <!-- MintonCSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
    <style>
    /*PARAMETROS DE ASIGNACIÖN*/
    .rojo{
        background-color: red;
        color:white;
    }
    .naranja{
        background-color:orange;
        color:white;
    }
    .amarilla{
        background-color:yellow;
        color:black;
    }
    .verde{
        background-color: green;
        color:white;
    }
    .morada{
        background-color: blueviolet;
        color:white;
    }
    .ocultar{
        display:none;
    }
    </style>
    <!-- end MintonCSS -->
</head>
<body class="widescreen fixed-left-void">
    <div id="wrapper" class="forced enlarged">
        <!-- incluir menu -->
        @include('layouts.menu')
        <!-- fin de menu -->
        @yield('content')
    </div>
    <!-- MintonJS -->
    <!-- Plugins  -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script><!-- Popper for Bootstrap -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/detect.js') }}"></script>
    <script src="{{ asset('js/fastclick.js') }}"></script>
    <script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('js/jquery.blockUI.js') }}"></script>
    <script src="{{ asset('js/waves.js') }}"></script>
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
    <script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('js/jquery.scrollTo.min.js') }}"></script>
    <!-- Custom main Js -->
    <script src="{{ asset('js/jquery.core.js') }}"></script>
    <script src="{{ asset('js/jquery.app.js') }}"></script>
    <!-- end MintonJS -->
</body>
</html>