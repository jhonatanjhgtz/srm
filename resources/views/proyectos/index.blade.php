@extends('layouts.app')

@section('title','Proyectos')

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0"></ol>
                        </div>
                        <h4 class="page-title">Proyectos</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <a href="{{ route('proyectos.create') }}" class="btn btn-success btn-custom waves-effect w-md waves-light m-b-5">Nuevo Proyecto</a>
            @include('common.success')
            <div class="row m-t-15">
                <div class="col-12">
                    <div class="card-box">
                        <div class="table-rep-plugin">
                            <div class="table-responsive" data-pattern="priority-columns">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Proyecto</th>
                                            <th>Nombre proyecto</th>
                                            <th>Estatus</th>
                                            <th>Editar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($proyectos as $proyecto)
                                        <tr>
                                            <td>{{ $proyecto->proproyecto }}</td>
                                            <td>{{ $proyecto->pronombre }}</td>
                                            <td><i class="fa fa-check text-center @if($proyecto->proestatus == 'Activo') text-success  @else text-muted @endif" title="{{ $proyecto->proestatus }}"></li></td>
                                            <td><a href="{{url('/proyectos/'.$proyecto->id.'/edit')}}" class="btn btn-primary btn-sm btn-custom waves-effect waves-light m-b-5" title="Editar"><li class="fa fa-edit"></li></a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection