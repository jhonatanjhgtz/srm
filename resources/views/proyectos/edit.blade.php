@extends('layouts.app')

@section('title','Editar Proyecto')
@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0"></ol>
                        </div>
                        <h4 class="page-title">Editar Proyecto</h4>
                        <div class="clearfix"></div>
                        <a href="{{ route('proyectos.index') }}"><i class="mdi mdi-keyboard-return"></i> Regresar</a>
                    </div>
                </div>
            </div>
            @include('common.errors')
            <div class="row">
                <div class="col-md-6 offset-md-3 m-t-20">
                    <div class="card-box">
                    {!! Form::model($proyecto, ['route' => ['proyectos.update', $proyecto->id], 'method' => 'PUT','files' => true]) !!}
                    @include('proyectos.form')
                    {!! Form::submit('Guardar',['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection