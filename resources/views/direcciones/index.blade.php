@extends('layouts.app')

@section('title','Direcciones')

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0"></ol>
                        </div>
                        <h4 class="page-title">Direcciones</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <a href="{{ route('direcciones.create') }}" class="btn btn-success btn-custom waves-effect w-md waves-light m-b-5">Nueva dirección</a>
            @include('common.success')
            <div class="row m-t-15">
                @foreach ($direcciones as $direccion)
                <div class="col-md-4">
                    <div class="widget-simple-chart text-right card-box text-center">
                        <i class="ti-agenda"></i>
                        <p class="text-muted text-nowrap m-b-10">{{$direccion->nombre}}</p>
                        <hr>
                        <a href="{{url('/direcciones/'.$direccion->id.'/edit')}}" class="btn btn-primary btn-sm btn-custom waves-effect w-md waves-light m-b-5">Editar</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection