@extends('layouts.app')

@section('title','Programas')

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0"></ol>
                        </div>
                        <h4 class="page-title">Programas</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <a href="{{ route('usuarios.create') }}" class="btn btn-success btn-custom waves-effect w-md waves-light m-b-5">Nuevo usuario</a>
            @include('common.success')
            <div class="row m-t-15">
                <div class="col-12">
                    <div class="card-box">
                        <div class="table-rep-plugin">
                            <div class="table-responsive" data-pattern="priority-columns">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Correo electrónico</th>
                                            <th>Rol</th>
                                            <th>Estatus</th>
                                            <th>Editar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($usuarios as $usuario)
                                        <tr>
                                            <td>{{ $usuario->name }}</td>
                                            <td>{{ $usuario->email }}</td>
                                            <td>{{ $usuario->rol }}</td>
                                            <td>{{ $usuario->estatus }}</td>
                                            <td>
                                                <a href="{{url('/usuarios/'.$usuario->id.'/edit')}}" class="btn btn-primary btn-sm btn-custom waves-effect waves-light m-b-5" title="Editar"><li class="fa fa-edit"></li></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection