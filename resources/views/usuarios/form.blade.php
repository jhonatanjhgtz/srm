<div class="form-group">
    {!! Form::label('name', 'Nombre') !!}
    {!! Form::text('name',null,['class' => 'form-control']) !!}
    {!! Form::label('email','Correo electrónico') !!}
    {!! Form::text('email',null,['class' => 'form-control']) !!}
    {!! Form::label('rol','Rol') !!}
    {!! Form::select('rol',['Administrador' => 'Administrador','General'=>'General'],null,['class'=>'form-control','placeholder'=>'Selecciona rol']) !!}
    {!! Form::label('id_direccion','Dirección') !!}
    {!! Form::select('id_direccion',$direcciones,null,['class' => 'form-control','placeholder'=>'Selecciona dirección']) !!}
    @if(isset($usuarios->id)) 
    {!! Form::label('estatus','Estatus') !!}
    {!! Form::select('estatus',['Activo' => 'Activo','Inactivo'=>'Inactivo'],null,['class'=>'form-control','placeholder'=>'Selecciona estatus']) !!}
    @endif
</div>