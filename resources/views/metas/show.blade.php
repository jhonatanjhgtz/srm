@extends('layouts.app')

@section('title','Detalle de meta')

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0"></ol>
                        </div>  
                        <h4 class="page-title">Meta detalle</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row m-t-15">
                <div class="col-12">
                    <div class="card-box">
                    @foreach ($infometa as $detalle_meta)
                            <p><strong>Dirección: </strong>{{ $detalle_meta->nombre }}</p>
                            <hr>
                            <p><strong>Programa: </strong>{{ $detalle_meta->progprograma }} - {{ $detalle_meta->prognombre }}</p>
                            <p><strong>Proyecto: </strong>{{ $detalle_meta->proproyecto }} - {{ $detalle_meta->pronombre }}</p>
                            <hr>
                            <p><strong>Meta: </strong>{{ $detalle_meta->memeta }}</p>
                            <p><strong>Nombre de la Acción: </strong>{{ $detalle_meta->menombre }}</p>
                            <p><strong>Unidad de Medida: </strong>{{ $detalle_meta->udnombre }}</p>
                        <hr>
                    @endforeach
                    <div class="table-responsive">
                        <table border="1" class="table">
                            <thead class="bg-success text-white text-center">
                                <tr>
                                    <th colspan="3">Anual</th>
                                </tr>
                                <tr>
                                    <th>Meta anual</th><th>Informe acumulado</th><th>Avance</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <td>{{ $infoAnual[0]->meta_anual }}</td><td>{{ $infoAnual[0]->informe_acumulado }}</td><td class="{{$desempeno}}">{{ number_format((float)$avance, 1, '.', '') }}%</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    @foreach ($infoPrimerTrimestre as $PrimerTrimestre)
                        <div class="table-responsive">
                            <table border="1">
                                <thead>
                                    <tr class="bg-success text-white">
                                        <th colspan="9" class="text-center">{{ $PrimerTrimestre->tritrimestre }}</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="2" class="text-center">Cantidad programada<br/>
                                            <th colspan="5" class="text-center">Realizado</th>
                                            <th colspan="2" class="text-center">Justificación trimestral en caso de ubicarse en las zonas: roja, amarilla, naranja o morada  (A)</th>
                                            <th colspan="2" rowspan="2" class="text-center">Descripción de la acción (B)</th>
                                        </th>
                                        <tr class="text-center">
                                            <td>enero</td>
                                            <td>febreo</td>
                                            <td>marzo</td>
                                            <td>Acumulado trimestral</td><td>Avance trimestral</td><td>Causas de la variación</td>
                                            <td>Medidas Correctivas</td>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td align="center"><a href="{{url('/trimestres/'.$PrimerTrimestre->id_trimestre.'/edit')}}" class="btn btn-success btn-sm btn-custom waves-effect waves-light m-b-5 text-center" title="Ver trimestre">
                                            @if($PrimerTrimestre->cantidad_rpogramada == "")
                                            Registrar    
                                            @else
                                            {{ $PrimerTrimestre->cantidad_rpogramada }}
                                            @endif</a>
                                        </td><!--Cantidad programada-->
                                        <td align="center">{{ $PrimerTrimestre->enero }}</td><!--mes1-->
                                        <td align="center">{{ $PrimerTrimestre->febrero }}</td><!--mes2-->
                                        <td align="center">{{ $PrimerTrimestre->marzo }}</td><!--mes3-->
                                        <td align="center">{{ $acumuelado_trimestralP = ($PrimerTrimestre->enero + $PrimerTrimestre->febrero + $PrimerTrimestre->marzo) }}</td><!--AcumuladoTrimestral-->
                                        <td align="center">@if($acumuelado_trimestralP !=0) {{ number_format((float)$acumuelado_trimestralP/$PrimerTrimestre->cantidad_rpogramada*100, 1, '.', '') }} @endif %</td><!--AvanceTrimestral-->
                                        <td></td><!--CausasVariación-->
                                        <td></td><!--MedidasCorrectivas-->
                                        <td></td><!--DescripciónAcciónB-->
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                    @endforeach
                    @foreach ($infoSegundoTrimestre as $SegundoTrimestre)
                        <div class="table-responsive">
                            <table border="1">
                                <thead>
                                    <tr class="bg-success text-white">
                                        <th colspan="9" class="text-center">{{ $SegundoTrimestre->tritrimestre }}</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="2" class="text-center">Cantidad programada<br/>
                                            <th colspan="5" class="text-center">Realizado</th>
                                            <th colspan="2" class="text-center">Justificación trimestral en caso de ubicarse en las zonas: roja, amarilla, naranja o morada  (A)</th>
                                            <th colspan="2" rowspan="2" class="text-center">Descripción de la acción (B)</th>
                                        </th>
                                        <tr class="text-center">
                                            <td>abril</td>
                                            <td>mayo</td>
                                            <td>junio</td>
                                            <td>Acumulado trimestral</td><td>Avance trimestral</td><td>Causas de la variación</td>
                                            <td>Medidas Correctivas</td>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td align="center"><a href="{{url('/trimestres/'.$SegundoTrimestre->id_trimestre.'/edit')}}" class="btn btn-success btn-sm btn-custom waves-effect waves-light m-b-5 text-center" title="Ver trimestre">
                                            @if($SegundoTrimestre->cantidad_rpogramada == "")
                                            Registrar    
                                            @else
                                            {{ $SegundoTrimestre->cantidad_rpogramada }}
                                            @endif</a>
                                        </td><!--Cantidad programada-->
                                        <td align="center">{{ $SegundoTrimestre->abril }}</td><!--mes1-->
                                        <td align="center">{{ $SegundoTrimestre->mayo }}</td><!--mes2-->
                                        <td align="center">{{ $SegundoTrimestre->junio }}</td><!--mes3-->
                                        <td align="center">{{ $acumuelado_trimestralS = ($SegundoTrimestre->abril + $SegundoTrimestre->mayo + $SegundoTrimestre->junio) }}</td><!--AcumuladoTrimestral-->
                                        <td align="center">@if($acumuelado_trimestralS !=0){{ number_format((float)$acumuelado_trimestralS/$SegundoTrimestre->cantidad_rpogramada*100, 1, '.', '') }} @endif%</td><!--AvanceTrimestral-->
                                        <td></td><!--CausasVariación-->
                                        <td></td><!--MedidasCorrectivas-->
                                        <td></td><!--DescripciónAcciónB-->
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                    @endforeach
                    @foreach ($infoTercerTrimestre as $TercerTrimestre)
                        <div class="table-responsive">
                            <table border="1">
                                <thead>
                                    <tr class="bg-success text-white">
                                        <th colspan="9" class="text-center">{{ $TercerTrimestre->tritrimestre }}</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="2" class="text-center">Cantidad programada<br/>
                                            <th colspan="5" class="text-center">Realizado</th>
                                            <th colspan="2" class="text-center">Justificación trimestral en caso de ubicarse en las zonas: roja, amarilla, naranja o morada  (A)</th>
                                            <th colspan="2" rowspan="2" class="text-center">Descripción de la acción (B)</th>
                                        </th>
                                        <tr class="text-center">
                                            <td>julio</td>
                                            <td>agosto</td>
                                            <td>septiembre</td>
                                            <td>Acumulado trimestral</td><td>Avance trimestral</td><td>Causas de la variación</td>
                                            <td>Medidas Correctivas</td>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td align="center"><a href="{{url('/trimestres/'.$TercerTrimestre->id_trimestre.'/edit')}}" class="btn btn-success btn-sm btn-custom waves-effect waves-light m-b-5 text-center" title="Ver trimestre">
                                        @if($TercerTrimestre->cantidad_rpogramada == "")
                                        Registrar    
                                        @else
                                        {{ $TercerTrimestre->cantidad_rpogramada }}
                                        @endif</a></td><!--Cantidad programada-->
                                        <td align="center">{{ $TercerTrimestre->julio }}</td><!--mes1-->
                                        <td align="center">{{ $TercerTrimestre->agosto }}</td><!--mes2-->
                                        <td align="center">{{ $TercerTrimestre->septiembre }}</td><!--mes3-->
                                        <td align="center">{{ $acumuelado_trimestralT = ($TercerTrimestre->julio + $TercerTrimestre->agosto + $TercerTrimestre->septiembre) }}</td><!--AcumuladoTrimestral-->
                                        <td align="center">@if($acumuelado_trimestralT!=0){{ number_format((float)$acumuelado_trimestralT/$TercerTrimestre->cantidad_rpogramada*100, 1, '.', '') }}@endif%</td><!--AvanceTrimestral-->
                                        <td></td><!--CausasVariación-->
                                        <td></td><!--MedidasCorrectivas-->
                                        <td></td><!--DescripciónAcciónB-->
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                    @endforeach
                    @foreach ($infoCuartoTrimestre as $CuartoTrimestre)
                        <div class="table-responsive">
                            <table border="1">
                                <thead>
                                    <tr class="bg-success text-white">
                                        <th colspan="9" class="text-center">{{ $CuartoTrimestre->tritrimestre }}</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="2" class="text-center">Cantidad programada<br/>
                                            <th colspan="5" class="text-center">Realizado</th>
                                            <th colspan="2" class="text-center">Justificación trimestral en caso de ubicarse en las zonas: roja, amarilla, naranja o morada  (A)</th>
                                            <th colspan="2" rowspan="2" class="text-center">Descripción de la acción (B)</th>
                                        </th>
                                        <tr class="text-center">
                                            <td>octubre</td>
                                            <td>noviembre</td>
                                            <td>diciembre</td>
                                            <td>Acumulado trimestral</td><td>Avance trimestral</td><td>Causas de la variación</td>
                                            <td>Medidas Correctivas</td>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td align="center"><a href="{{url('/trimestres/'.$CuartoTrimestre->id_trimestre.'/edit')}}" class="btn btn-success btn-sm btn-custom waves-effect waves-light m-b-5 text-center" title="Ver trimestre">
                                            @if($CuartoTrimestre->cantidad_rpogramada == "")
                                            Registrar    
                                            @else
                                            {{ $CuartoTrimestre->cantidad_rpogramada }}
                                            @endif</a>
                                        </td><!--Cantidad programada-->
                                        <td align="center">{{ $CuartoTrimestre->octubre }}</td><!--mes1-->
                                        <td align="center">{{ $CuartoTrimestre->noviembre }}</td><!--mes2-->
                                        <td align="center">{{ $CuartoTrimestre->diciembre }}</td><!--mes3-->
                                        <td align="center">{{ $acumuelado_trimestralC = $CuartoTrimestre->octubre + $CuartoTrimestre->noviembre + $CuartoTrimestre->diciembre }}</td><!--AcumuladoTrimestral-->
                                        <td align="center">@if($acumuelado_trimestralC!=0){{ number_format((float)$acumuelado_trimestralC/$CuartoTrimestre->cantidad_rpogramada*100, 1, '.', '') }}@endif%</td><!--AvanceTrimestral-->
                                        <td></td><!--CausasVariación-->
                                        <td></td><!--MedidasCorrectivas-->
                                        <td></td><!--DescripciónAcciónB-->
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection