@extends('layouts.app')

@section('title','Metas')

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0"></ol>
                        </div>
                        <h4 class="page-title">Metas</h4>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <a href="{{ route('metas.create') }}" class="btn btn-success btn-custom waves-effect w-md waves-light m-b-5">Nueva Meta</a>
            @include('common.success')
            <div class="row m-t-15">
                <div class="col-12">
                    <div class="card-box">
                        <div class="table-rep-plugin">
                            <div class="table-responsive" data-pattern="priority-columns">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Dirección</th>
                                            <th>Programa</th>
                                            <th>Proyecto</th>
                                            <th>Meta</th>
                                            <th>Unidad de medida</th>
                                            <th>Detalle</th>
                                            <th>Editar</th>
                                        </tr>
                                    </thead>
                                    @foreach ($infometas as $infometa)
                                        <tbody>
                                            <td>{{$infometa->nombre_direccion}}</td>
                                            <td>{{$infometa->prognombre}}</td>
                                            <td>{{$infometa->pronombre}}</td>
                                            <td>{{$infometa->memeta}}</td>
                                            <td>{{$infometa->udnombre}}</td>
                                            <td>
                                                <a href="{{url('/metas/'.$infometa->id_meta.'')}}" class="btn btn-primary btn-sm btn-custom waves-effect waves-light m-b-5" title="Ver detalle de meta"><i class="mdi mdi-open-in-new"></i></a>
                                            </td>
                                            <td>
                                                <a href="{{url('/metas/'.$infometa->id_meta.'/edit')}}" class="btn btn-primary btn-sm btn-custom waves-effect waves-light m-b-5" title="Editar"><li class="fa fa-edit">
                                            </td>
                                        </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection