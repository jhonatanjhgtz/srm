<div class="form-group">
    {!! Form::label('memeta', 'Meta') !!}
    {!! Form::text('memeta',null,['class' => 'form-control']) !!}
    {!! Form::label('menombre','Nombre de la Acción') !!}
    {!! Form::text('menombre',null,['class' => 'form-control']) !!}
    {!! Form::label('id_unidad_medida','Unidad de medida') !!}
    {!! Form::select('id_unidad_medida', $unidades->pluck('udnombre','id'),null,['class'=>'form-control','placeholder'=>'Seleccionar']) !!}
    {!! Form::label('id_direccion','Dirección') !!}
    {!! Form::select('id_direccion', $direcciones->pluck('nombre','id'),null,['class'=>'form-control','placeholder'=>'Seleccionar']) !!}
    {!! Form::label('id_programa','Programa') !!}
    {!! Form::select('id_programa', $programas->pluck('prognombre','id'),null,['class'=>'form-control','placeholder'=>'Seleccionar']) !!}
    {!! Form::label('id_proyecto','proyecto') !!}
    {!! Form::select('id_proyecto', $proyectos->pluck('pronombre','id'),null,['class'=>'form-control','placeholder'=>'Seleccionar']) !!}
</div>