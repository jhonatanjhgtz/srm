@extends('layouts.start')

@section('title','Recuperar contraseña')

@section('content')

<div class="">
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        ¡Ingresa tu <b>Correo electrónico</b> y sigue las instrucciones para restaurar tu contraseña!
    </div>
</div>
<div class="card">
    <div class="card-header">
        <img src="{{ asset('/images/logo_login.png') }}" alt="" width="320">
    </div>

    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
            @csrf

            <div class="form-group row">
                <div class="col-12">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="mdi mdi-account"></i></span>
                        </div>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="correo eléctronico" required>
                        <span class="input-group-append"> <button type="submit" class="btn btn-success  btn-custom waves-effect waves-light">Enviar</button> </span>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group row mb-0">
                <div class="col-12"><a href="{{ route('login') }}"><i class="mdi mdi-keyboard-return"></i> Regresar</a></div>
            </div>

        </form>
    </div>
</div>
@endsection
