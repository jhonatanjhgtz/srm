@extends('layouts.app')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@section('title','Trimestre')
@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group pull-right">
                            <ol class="breadcrumb hide-phone p-0 m-0"></ol>
                        </div>
                        <h4 class="page-title">{{ $meta_allData[0]->tritrimestre}}</h4>
                        <div class="clearfix"></div>
                        <a href="{{ route('metas.index') }}"><i class="mdi mdi-keyboard-return"></i> Regresar</a>
                    </div>
                </div>
            </div>
            @include('common.errors')
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <p><strong>Dirección: </strong>{{ $meta_allData[0]->nombre_direccion }} <strong>Programa: </strong>{{ $meta_allData[0]->progprograma }} - {{ $meta_allData[0]->prognombre }}</p>
                        <p><strong>Proyecto: </strong>{{ $meta_allData[0]->proproyecto }} - {{ $meta_allData[0]->pronombre }}</p>
                        <p><strong>Meta: </strong>{{ $meta_allData[0]->memeta }}</p>
                        <p><strong>Nombre de la Acción: </strong>{{ $meta_allData[0]->menombre }}</p>
                        <p><strong>Unidad de Medida: </strong>{{ $meta_allData[0]->udnombre }}</p>  
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card-box">
                        {!! Form::model($trimestre, ['route' => ['trimestres.update', $trimestre->id], 'method' => 'PUT','files' => true]) !!}
                            @include('trimestres.form')
                        {!! Form::submit('Guardar',['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card-box">
                        @if ($trimestre->tricantidad_rpogramada != "")
                            <div id="refreshCantidadprogramada" class="col-md-12"></div>
                        @if (count($archivos_trimestre)!=3)
                            <div class="form-group upload_form">
                                {!! Form::open(['route' => 'uploadFile','method' => 'POST','class' => 'form-group','files' => true, 'id' => 'formFile']) !!}
                                    {!! Form::hidden('id_trimestre',$trimestre->id,['id'=>'id_trimestre']) !!}
                                    {!! Form::label('archnombre','Evidencia') !!}
                                    {!! Form::file('archnombre',['id'=>'archnombre']) !!}
                                    {!! Form::label('cantidad_programada_mes','Cantidad programada por mes:') !!}
                                    {!! Form::text('cantidad_programada_mes',null,['class' => 'form-control']) !!}
                                    <button type="submit" class="btn btn-danger btn-sm" id="uploadFile" title="Agregar"><i class="mdi mdi-file-plus"></i>Agregar</button>
                                {{ Form::close() }}
                            </div>
                        @endif
                            <div id="refreshFilesTrimestre" class="col-md-12">
                                @include('trimestres.listadoArchivos')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
$(document).ready(function(){
    $('#uploadFile').click(function(e){
        e.preventDefault();

        var url = $('#formFile').attr('action');
        //var form = $(this).parents('form');
        var formData = new FormData($('#formFile') [0]);
        var token = $('input[name=_token]').val();
        
        $.ajax({
            url: url,
            headers: {'X-CSRF-TOKEN':token},
            type: 'post',
            contentType: false,
            processData: false,
            data: formData,
            success:function(data)
            {
                $('#refreshFilesTrimestre').html(data);
            }
        });
        $('#archnombre').val("");
    });
    $('input[name="trireconduccion_skip"]').click(function(){
        var reconduccionValue = $('input[name="trireconduccion_skip"]:checked').val();
        if(reconduccionValue =="Si"){
            $('#show_reconduccion_file').show();
        }else if(reconduccionValue =="No"){
            $('#show_reconduccion_file').hide();
            $('#trirereconduccion').val("");
        }
    });
});
</script>