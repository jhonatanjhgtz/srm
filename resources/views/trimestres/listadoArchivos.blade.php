@if (count($archivos_trimestre))
<h4 class="page-title">Unidad de medida</h4><br>
<table>
    <tbody>
        <tr>
            <th style="display:none;"></th><th></td><th></th><th></th>
        </tr>
    @foreach ($archivos_trimestre as $listado_archivo)
        <tr>
            <td style="display:none;" class="id_file_carga">{{$listado_archivo->id}}</td>
            <td>Cantidad por mes: <b>{{$listado_archivo->cantidad_programada_mes}}</b></td>
            <td><a href="{{asset("public/files/$listado_archivo->archnombre")}}" target="_blank"><i class="mdi mdi-file text-success" title="Ver archivo"></i></a></td>
            <td class="editCantidad"><a href="#" title="Editar"><i class="fa fa-edit"></i></a></td>
        </tr>
    {!! Form::open(['route' => ['deleteFile',$listado_archivo->id], 'method' => 'DELETE', 'class' => 'formDelete']) !!}
        {!!Form::hidden('archnombre',$listado_archivo->archnombre,['id' => 'archnombre'])!!}
        {!! Form::hidden('id_trimestre',$listado_archivo->id_trimestre,['id'=>'id_trimestre']) !!}
        <!--<a href="#" class="delete_File text-danger" title="Eliminar archivo"><i class="mdi mdi-delete-forever"></i></a>-->
    {!! Form::close() !!}
    @endforeach
    </tbody>
</table>
<script>
$(document).ready(function(){
    /*$('.delete_File').on('click',function(e){
        //console.log("Quiere eliminar prro! xD");
        e.preventDefault();
        var url = $('.formDelete').attr('action');
        var form = $(this).parents($('.formDelete'));

        if(! confirm("¿Esta seguro de eliminar este archivo?")){
            return false;
        }
        $.post(url,form.serialize(),function(result){
            $('#refreshFilesTrimestre').html(result);
        }).fail(function(){
            console.log("Ocurrio algo inesperado :O!!");
        });
    });*/
    $('.editCantidad').on('click',function(){
        $('.upload_form').hide();
        var id = "";
        $(this).parents("tr").find(".id_file_carga").each(function(){
            id += $(this).html();
        });
        //e.preventDefault();
        var url = 'http://192.168.100.58/SRM/public/index.php/programado/'+id+'/edit';
        //var form = $(this).parents('form');
        var formData = new FormData($('#edit') [0]);
        var token = $('input[name=_token]').val();
         
        $.ajax({
            url: url,
            headers: {'X-CSRF-TOKEN':token},
            type: 'get',
            contentType: false,
            processData: false,
            data: formData,
            success:function(data)
            {
                $('#refreshCantidadprogramada').html(data);
            }
        });
    });
});
</script>
@endif