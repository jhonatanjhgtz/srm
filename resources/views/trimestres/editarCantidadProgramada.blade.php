<div class="form-group">
    {!! Form::model($cantidadProgramada, ['route' => ['programado.update', $cantidadProgramada->id], 'method' => 'PUT', 'class' => 'form-group','files' => true, 'id' => 'edit']) !!}
        {!! Form::hidden('id',$cantidadProgramada->id,['id'=>'id']) !!}
        {!! Form::label('archnombre_','Evidencia') !!}
        {!! Form::file('archnombre_',['id'=>'archnombre_']) !!}
        {!! Form::hidden('archnombre',null) !!}
        {!! Form::label('cantidad_programada_mes','Cantidad programada por mes:') !!}
        {!! Form::text('cantidad_programada_mes',null,['class' => 'form-control']) !!}
        <button type="submit" class="btn btn-danger btn-sm" title="Guardar"><i class="mdi mdi-file-plus"></i>Guardar</button>
    {{ Form::close() }}
</div>