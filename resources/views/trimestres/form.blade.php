<div class="form-group">
    @if($meta_allData[0]->trireconduccion=="Si")
        {!! Form::label('reconduccion_skip','Ver Reconducción',['class'=>'text-danger']) !!}
        <a href="{{asset("public/files/".$meta_allData[0]->reconduccion."")}}" target="_blank"><i class="mdi mdi-file text-success" title="Ver archivo"></i></a></br>
    @endif
    @if(Auth::user()->rol == 'Administrador')
    <div id="show_reconduccion_file" class="ocultar">
        {!! Form::label('trirereconduccion','Reconducción:') !!}
        {!! Form::file('trirereconduccion',['id'=>'trirereconduccion']) !!}
        {!! Form::hidden('cantidad_programada_mes_skip',$trimestre->tricantidad_rpogramada) !!}
    </div>
    {!! Form::label('reconduccionlabel','Generar Reconducción:') !!}
    Si {!! Form::radio('trireconduccion_skip','Si',['class'=>'form-control'])!!} No
    {!! Form::radio('trireconduccion_skip','No',['class'=>'form-control'])!!}
    <br>
    @endif
    {!! Form::label('tricantidad_rpogramada', 'Cantidad programada') !!}   
    {!! Form::text('tricantidad_rpogramada',null,['class' => 'form-control', 'id' => 'demo3','style' => 'display: block;']) !!}
    {!! Form::hidden('trireconduccion',$meta_allData[0]->reconduccion) !!}
    {!! Form::label('tricausas_variacion','Causas de la variación',['title' => 'Justificación en caso de presentar un avance trimestral inferior a 90% o mayor a 110%  (A)']) !!}
    {!! Form::textarea('tricausas_variacion',null,['class'=>'form-control', 'rows' => 2, 'cols' => 40]) !!}
    {!! Form::label('trimedidas_correctivas','Medidas Correctivas') !!}
    {!! Form::textarea('trimedidas_correctivas',null,['class'=>'form-control', 'rows' => 2, 'cols' => 40]) !!}
    {!! Form::label('triaccionb','Descripción de la acción (B)') !!}
    {!! Form::textarea('triaccionb',null,['class'=>'form-control', 'rows' => 2, 'cols' => 40]) !!}
</div>