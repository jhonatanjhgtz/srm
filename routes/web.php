<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Auth\LoginController@showLoginForm')->middleware('guest');
//name=formulario
Auth::routes();

//ruta para login name="nombre de la rura"
Route::post('login','Auth\LoginController@login')->name('login');

Route::get('logout', function(){

    return back();

});

//Route::post('logout','Auth\LoginController@logout')->name('logout');

Route::group(['middleware'=>'auth'],function(){
    Route::get('/home', 'HomeController@index')->name('home');
});

Route::group(['middleware'=>'auth'],function(){
    Route::resource('direcciones','direccionController');
});

Route::group(['middleware'=>'auth'],function(){
    Route::resource('unidades_medida','unidadMedidaController');
});

Route::group(['middleware'=>'auth'],function(){
    Route::resource('programas','programaController');
});

Route::group(['middleware'=>'auth'],function(){
    Route::resource('proyectos','proyectoController');
});

Route::group(['middleware'=>'auth'],function(){
    Route::resource('metas','metaController');
});

Route::group(['middleware'=>'auth'],function(){
    Route::resource('trimestres','trimestreController');
});

Route::group(['middleware'=>'auth'],function(){
    Route::post('trimestres','trimestreController@uploadFile')->name('uploadFile');
});

Route::group(['middleware'=>'auth'],function(){
    Route::delete('trimestres{id}','trimestreController@deleteFileTirmeste')->name('deleteFile');
});

Route::group(['middleware'=>'auth'],function(){
    Route::get('trimestres{id}','trimestreController@edit');
});

Route::group(['middleware'=>'auth'],function(){
    Route::resource('programado','trimestreArchivoController');
});

Route::group(['middleware'=>'auth'],function(){
    Route::resource('usuarios','usuarioController');
});
